# -*- coding: iso-8859-1 -*-
from requests import get
from bs4 import BeautifulSoup
import json
import re

class RobotEsaj:

    url = 'https://esaj.tjsp.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=0165643-21.2011&foroNumeroUnificado=0100&dadosConsulta.valorConsultaNuUnificado='

    processo = []

    def crawl_cnjs(self, cnjs):                
        for cnj in cnjs:
            codigo = re.sub('[.-]','',cnj)
            path = self.url+codigo
            response = get(path, verify=False)
            html_soup = BeautifulSoup(response.text, 'html.parser')
            self.processo.append(self.extrair_processo(cnj, html_soup))            
        return self.processo

    def crawl_html(self, cnj, html_doc):
        html_soup = BeautifulSoup(html_doc, 'html.parser')        
        self.processo.append(self.extrair_processo(cnj, html_soup))        
        return self.processo

    def extrair_processo(self, cnj, html_soup):
        processo = dict({'numero': cnj,'dadosProcesso': self.extrair_dados_processo(html_soup),'partesProcesso': self.extrair_partes_processo(html_soup), 'movimentacoes' : self.extrair_movimentacoes(html_soup), 'peticoes': self.extrair_peticoes(html_soup), 'incidentes' : self.extrair_incidentes(html_soup), 'apensos': self.extrair_apensos(html_soup), 'audiencias' : self.extrair_audiencias(html_soup), 'historico': self.extrair_historico(html_soup)})     
        return processo

    def extrair_dados_processo(self, html_soup):

        dado = dict()

        element = html_soup.find_all('table', class_="secaoFormBody")[1]        

        if element is not None:

            for tr in element.find_all('tr', class_=""):

                if tr.find('td', width="150") is not None:
                    tdLabel = tr.find('td', width="150")
                    lbl = tdLabel.label if tdLabel is not None else ''
                    label = lbl.get_text("", strip=True) if lbl is not None else 'Localizcao'
                    value = ''
                    content = tdLabel.next_sibling.next_sibling.children

                    for teste in content:
                        if teste.name == 'table':
                            teste2 = teste.tr.td.get_text('',strip=True).split(':') if teste.tr.td.get_text('',strip=True).find(':') != -1 else teste.tr.td.get_text(' ',strip=True)
                            if isinstance(teste2, list):
                                label = teste2[0]
                                value = teste2[1]
                            else:
                                value = teste2                
                        if teste.name == 'span':
                            value = teste.get_text().strip()

                    dado[label] = value                
                    
        return dado

    def extrair_partes_processo(self, html_soup):
        tbPartesPrincipais = html_soup.find('table', id="tablePartesPrincipais")
        tbTodasAsPartes = html_soup.find('table', id="tableTodasPartes")  
        partes = []
        if tbTodasAsPartes:        
            partes = self.extrair_detail_partes_processo(tbTodasAsPartes)
        else:
            partes = self.extrair_detail_partes_processo(tbPartesPrincipais)
            
        return partes

    def extrair_detail_partes_processo(self,element):
        partes = []
        if element is not None:
            for tr in element.find_all('tr', class_="fundoClaro"):            
                lbl = tr.td.span if tr.td is not None else ''
                label = lbl.get_text("", strip=True).replace(':','') if lbl is not None else ''            

                if label.find('.') != -1:
                    label = label.replace('.','')

                content = tr.td.next_sibling.next_sibling.get_text('|',strip=True)
                
                dado = dict()

                if(content.find(':|') != -1):
                    content2 = content.replace(':|',':')
                else:
                    content2 = content
                
                content3 = content2.split('|')            
                
                pessoas  = []

                for content4 in content3:
                    
                    dado[label] = content3[0]

                    if(content4.find(':') != -1):                                                

                        representantes = content4.split(':')

                        if representantes[0].find('.') != -1:
                            representantes[0] = representantes[0].replace('.','')

                        pessoas.append(dict({representantes[0]: representantes[1]}))
                        
                dado['prepostos'] = pessoas
                partes.append(dado)

        return partes

    def extrair_movimentacoes(self, html_soup):
        tbUltimasMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes")
        tbTodasMovimentacoes = html_soup.find('tbody', id="tabelaTodasMovimentacoes")  
        movimentacoes = []

        if tbTodasMovimentacoes:        
            movimentacoes = self.extrair_detail_movimentacoes(tbTodasMovimentacoes)
        else:
            movimentacoes = self.extrair_detail_movimentacoes(tbUltimasMovimentacoes)
            
        return movimentacoes            

    def extrair_detail_movimentacoes(self, element):
        movimentacoes = []        
        if element is not None:            
            for tr in element.find_all('tr', style=""):                
                tdData = tr.td
                tdDocumento = tdData.next_sibling.next_sibling
                tdMovimentacao = tdDocumento.next_sibling.next_sibling
                
                mov = dict()

                mov['data'] = tdData.get_text('', strip=True)

                if tdDocumento.a :
                    mov['documento'] = tdDocumento.a['href']
                
                if(tdMovimentacao.span):
                    valorMovimentacao = tdMovimentacao.span.extract().get_text('', strip=True)
                    tipoMovimento = tdMovimentacao.get_text('', strip=True)                    
                    mov['tipoMovimentacao'] = tipoMovimento
                    mov['valorMovimentacao'] = valorMovimentacao                    

                movimentacoes.append(mov)
        
        return movimentacoes

    def extrair_peticoes(self, html_soup):        
        tbMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes").parent
        siblings = []
        peticoes = []

        for sibling in tbMovimentacoes.next_siblings:
            if sibling != '\n':
                siblings.append(sibling)

        for tr in siblings[2].find_all('tr'):
            if tr.td is not None and tr.td.get_text('',strip=True) != '' and tr.td.get_text('',strip=True).find('N�o') == -1:  
                cell = tr.td
                data = cell.get_text('', strip=True)
                tipo = cell.next_sibling.next_sibling.get_text('', strip=True)
                peticao = dict({'data': data, 'tipo': tipo})
                peticoes.append(peticao)
        
        return peticoes

    def extrair_incidentes(self, html_soup):        
        tbMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes").parent
        siblings = []

        for sibling in tbMovimentacoes.next_siblings:
            if sibling != '\n':
                siblings.append(sibling)

        incidentes = []

        for tr in siblings[6].find_all('tr'):
            if tr.td is not None and tr.td.get_text('',strip=True) != '' and tr.td.get_text('',strip=True).find('N�o') == -1:                
                cell = tr.td
                data  = cell.get_text('', strip=True)
                classe = cell.next_sibling.next_sibling.get_text('', strip=True)
                incidente = dict({'data': data, 'classe': classe})
                incidentes.append(incidente)

        return incidentes
    
    #testar depois com exemplo que possua apensos
    def extrair_apensos(self, html_soup):
        tbMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes").parent
        siblings = []

        for sibling in tbMovimentacoes.next_siblings:
            if sibling != '\n':
                siblings.append(sibling)

        apensos = []

        # for tr in siblings[10].find_all('tr'):
        #     if tr.td is not None and tr.td.get_text('',strip=True) != '' and tr.td.get_text('',strip=True).find('N�o') == -1:                
        #         cell = tr.td
        #         data  = cell.get_text('', strip=True)
        #         classe = cell.next_sibling.next_sibling.get_text('', strip=True)
        #         apenso = dict({'data': data, 'classe': classe})
        #         apensos.append(apenso)

        return apensos        

    def extrair_audiencias(self, html_soup):
        tbMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes").parent
        siblings = []
        audiencias = []

        for sibling in tbMovimentacoes.next_siblings:
            if sibling != '\n':
                siblings.append(sibling)

        for tr in siblings[13].find_all('tr'):
            if tr.td is not None and tr.td.get_text('',strip=True) != '' and tr.td.get_text('',strip=True).find('N�o') == -1:                
                data = tr.td
                audiencia = data.next_sibling.next_sibling
                situacao = audiencia.next_sibling.next_sibling
                qtdPessoas = situacao.next_sibling.next_sibling
                a = dict({'data': data.get_text('', strip=True), 'audiencia': audiencia.get_text('', strip=True), 'situacao': situacao.get_text('', strip=True), 'qtdPessoas' : qtdPessoas.get_text('', strip=True)})
                audiencias.append(a)

        return audiencias

    def extrair_historico(self, html_soup):
        tbMovimentacoes = html_soup.find('tbody', id="tabelaUltimasMovimentacoes").parent
        siblings = []
        historico = []

        for sibling in tbMovimentacoes.next_siblings:
            if sibling != '\n':
                siblings.append(sibling)

        for tr in siblings[15].find_all('tr'):
            if tr.td is not None and tr.td.get_text('',strip=True) != '' and tr.td.get_text('',strip=True).find('N�o') == -1:
                data = tr.td
                tipo = data.next_sibling.next_sibling
                classe = tipo.next_sibling.next_sibling
                area = classe.next_sibling.next_sibling
                motivo = area.next_sibling.next_sibling
                h = dict({'data': data.get_text('', strip=True), 'tipo': tipo.get_text('', strip=True), 'classe': classe.get_text('', strip=True),'area':area.get_text('', strip=True), 'motivo' : motivo.get_text('', strip=True)})
                historico.append(h)

        return historico

    def log_siblings(self, container):
        for i in range(0, len(container), 1):
            print(i)
            print(container[i])

    def log_esaj(self, param):
        print(param)
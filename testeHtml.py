
# def html():
#     return html_doc = """
# <span class="esajTituloOrientacoes">Orienta��es</span>
# <ul class="esajUlOrientacoes" id="">
#   <li>
#     Processos distribu�dos no mesmo dia podem ser localizados se buscados pelo n�mero do processo, com o seu foro selecionado.
#   </li>
#   <li>
#     Algumas unidades dos foros listados abaixo n�o est�o dispon�veis para consulta. Para saber quais varas est�o dispon�veis em cada foro clique <a style="cursor:pointer" class="layout" onclick="popup('/WebHelp/id_varas_com_pesquisa_processual_disponivel.htm','','location=no, toolbar=no, resizable=yes, width=795, height=560, scrollbars=yes')">aqui</a>.
#   </li>
#   <li>
#     D�vidas? Clique <a style="cursor:pointer" class="layout" onclick="popup('/WebHelp/id_consultas_processuais.htm','','location=no, toolbar=no, resizable=yes, width=795, height=560, scrollbars=yes')">aqui</a> para mais informa��es sobre como pesquisar.
#   </li>
#   <li>
#     Processos baixados, em segredo de justi�a ou distribu�dos no mesmo dia ser�o apresentados somente na pesquisa pelo n�mero do processo.
#   </li>
# </ul>
# <br>
# <form name="consultarProcessoForm" method="GET" action="/cpopg/search.do" autocomplete="off" enctype="" onsubmit="return applySubmit(this, eval('function spwSubmit(t, e){decodificaInputMulSelOnSubmit();if (!IS_enableSubmit) return false; return BENV_isCamposValidos(t); } spwSubmit(this, event);'));" id="formConsulta" target="" novalidate="">
#   <input type="hidden" name="conversationId" value="">
#   <div class="">
#     <br>
#     <table width="100%" border="0" cellspacing="0" cellpadding="0">
#       <tbody>
#         <tr valign="top">
#           <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#             <h2 class="subtitle">
#               Dados para pesquisa
#             </h2>
#           </td>
#           <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#             <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#           </td>
#         </tr>
#       </tbody>
#     </table>
#     <br>
#     <table id="secaoFormConsulta" class="secaoFormBody" width="100%" style="" cellpadding="2" cellspacing="0" border="0">
#       <tbody>
#         <tr class="">
#           <td id="" width="150" valign="">
#             <label for="id_Foro" class="" style="text-align:right;font-weight:bold;;">Foro:</label>
#           </td>
#           <td valign="">
#             <select name="dadosConsulta.localPesquisa.cdLocal" id="id_Foro" obrigatorio="" rotulo="Foro">
#               <option value="-1">
#                 Todos os foros da lista abaixo
#               </option>
#               <option value="91">ANTIGO Foro Distrital de Br�s Cubas</option>
#               <option value="509">Ara�atuba/DEECRIM UR2</option>
#               <option value="26">Bauru/DEECRIM UR3</option>
#               <option value="502">Campinas/DEECRIM UR4</option>
#               <option value="500">DEPRE</option>
#               <option value="53">Foro Central - Fazenda P�blica/Acidentes</option>
#               <option value="100" selected="selected">Foro Central C�vel</option>
#               <option value="52">Foro Central Criminal - Juri</option>
#               <option value="50">Foro Central Criminal Barra Funda</option>
#               <option value="16">Foro Central Juizados Especiais C�veis</option>
#               <option value="800">Foro da Comiss�o Processante Permanente</option>
#               <option value="14">Foro das Execu��es Fiscais Estaduais</option>
#               <option value="90">Foro das Execu��es Fiscais Municipais</option>
#               <option value="81">Foro de Adamantina</option>
#               <option value="83">Foro de Agua�</option>
#               <option value="35">Foro de �guas de Lindoia</option>
#               <option value="58">Foro de Agudos</option>
#               <option value="42">Foro de Altin�polis</option>
#               <option value="19">Foro de Americana</option>
#               <option value="40">Foro de Am�rico Brasiliense</option>
#               <option value="22">Foro de Amparo</option>
#               <option value="24">Foro de Andradina</option>
#               <option value="25">Foro de Angatuba</option>
#               <option value="28">Foro de Aparecida</option>
#               <option value="30">Foro de Apia�</option>
#               <option value="32">Foro de Ara�atuba</option>
#               <option value="37">Foro de Araraquara</option>
#               <option value="38">Foro de Araras</option>
#               <option value="666">Foro de Artur Nogueira</option>
#               <option value="45">Foro de Aruj�</option>
#               <option value="47">Foro de Assis</option>
#               <option value="48">Foro de Atibaia</option>
#               <option value="60">Foro de Auriflama</option>
#               <option value="73">Foro de Avar�</option>
#               <option value="59">Foro de Bananal</option>
#               <option value="62">Foro de Bariri</option>
#               <option value="63">Foro de Barra Bonita</option>
#               <option value="66">Foro de Barretos</option>
#               <option value="68">Foro de Barueri</option>
#               <option value="69">Foro de Bastos</option>
#               <option value="70">Foro de Batatais</option>
#               <option value="71">Foro de Bauru</option>
#               <option value="72">Foro de Bebedouro</option>
#               <option value="75">Foro de Bertioga</option>
#               <option value="76">Foro de Bilac</option>
#               <option value="77">Foro de Birigui</option>
#               <option value="82">Foro de Boituva</option>
#               <option value="67">Foro de Borborema</option>
#               <option value="79">Foro de Botucatu</option>
#               <option value="99">Foro de Bragan�a Paulista</option>
#               <option value="94">Foro de Brodowski</option>
#               <option value="95">Foro de Brotas</option>
#               <option value="691">Foro de Buri</option>
#               <option value="97">Foro de Buritama</option>
#               <option value="80">Foro de Cabre�va</option>
#               <option value="101">Foro de Ca�apava</option>
#               <option value="102">Foro de Cachoeira Paulista</option>
#               <option value="103">Foro de Caconde</option>
#               <option value="104">Foro de Cafel�ndia</option>
#               <option value="106">Foro de Caieiras</option>
#               <option value="108">Foro de Cajamar</option>
#               <option value="111">Foro de Cajuru</option>
#               <option value="114">Foro de Campinas</option>
#               <option value="115">Foro de Campo Limpo Paulista</option>
#               <option value="116">Foro de Campos do Jord�o</option>
#               <option value="118">Foro de Canan�ia</option>
#               <option value="120">Foro de C�ndido Mota</option>
#               <option value="123">Foro de Cap�o Bonito</option>
#               <option value="125">Foro de Capivari</option>
#               <option value="126">Foro de Caraguatatuba</option>
#               <option value="127">Foro de Carapicu�ba</option>
#               <option value="128">Foro de Cardoso</option>
#               <option value="129">Foro de Casa Branca</option>
#               <option value="132">Foro de Catanduva</option>
#               <option value="136">Foro de Cerqueira C�sar</option>
#               <option value="137">Foro de Cerquilho</option>
#               <option value="140">Foro de Chavantes</option>
#               <option value="142">Foro de Colina</option>
#               <option value="144">Foro de Conchal</option>
#               <option value="145">Foro de Conchas</option>
#               <option value="146">Foro de Cordeir�polis</option>
#               <option value="150">Foro de Cosm�polis</option>
#               <option value="152">Foro de Cotia</option>
#               <option value="153">Foro de Cravinhos</option>
#               <option value="156">Foro de Cruzeiro</option>
#               <option value="157">Foro de Cubat�o</option>
#               <option value="159">Foro de Cunha</option>
#               <option value="160">Foro de Descalvado</option>
#               <option value="161">Foro de Diadema</option>
#               <option value="165">Foro de Dois C�rregos</option>
#               <option value="168">Foro de Dracena</option>
#               <option value="169">Foro de Duartina</option>
#               <option value="172">Foro de Eldorado Paulista</option>
#               <option value="176">Foro de Embu das Artes</option>
#               <option value="177">Foro de Embu-Gua�u</option>
#               <option value="180">Foro de Esp�rito Santo do Pinhal</option>
#               <option value="185">Foro de Estrela D'Oeste</option>
#               <option value="187">Foro de Fartura</option>
#               <option value="189">Foro de Fernand�polis</option>
#               <option value="191">Foro de Ferraz de Vasconcelos</option>
#               <option value="673">Foro de Fl�rida Paulista</option>
#               <option value="196">Foro de Franca</option>
#               <option value="197">Foro de Francisco Morato</option>
#               <option value="198">Foro de Franco da Rocha</option>
#               <option value="200">Foro de G�lia</option>
#               <option value="201">Foro de Gar�a</option>
#               <option value="204">Foro de General Salgado</option>
#               <option value="205">Foro de Getulina</option>
#               <option value="210">Foro de Gua�ra</option>
#               <option value="213">Foro de Guar�</option>
#               <option value="218">Foro de Guararapes</option>
#               <option value="219">Foro de Guararema</option>
#               <option value="220">Foro de Guaratinguet�</option>
#               <option value="222">Foro de Guariba</option>
#               <option value="223">Foro de Guaruj�</option>
#               <option value="224">Foro de Guarulhos</option>
#               <option value="229">Foro de Hortol�ndia</option>
#               <option value="27">Foro de Iacanga</option>
#               <option value="233">Foro de Ibat�</option>
#               <option value="236">Foro de Ibitinga</option>
#               <option value="238">Foro de Ibi�na</option>
#               <option value="240">Foro de Iep�</option>
#               <option value="242">Foro de Igarapava</option>
#               <option value="244">Foro de Iguape</option>
#               <option value="246">Foro de Ilha Solteira</option>
#               <option value="247">Foro de Ilhabela</option>
#               <option value="248">Foro de Indaiatuba</option>
#               <option value="252">Foro de Ipau�u</option>
#               <option value="257">Foro de Ipu�</option>
#               <option value="262">Foro de Itaber�</option>
#               <option value="263">Foro de Ita�</option>
#               <option value="264">Foro de Itajobi</option>
#               <option value="266">Foro de Itanha�m</option>
#               <option value="268">Foro de Itapecerica da Serra</option>
#               <option value="269">Foro de Itapetininga</option>
#               <option value="270">Foro de Itapeva</option>
#               <option value="271">Foro de Itapevi</option>
#               <option value="272">Foro de Itapira</option>
#               <option value="274">Foro de It�polis</option>
#               <option value="275">Foro de Itaporanga</option>
#               <option value="278">Foro de Itaquaquecetuba</option>
#               <option value="279">Foro de Itarar�</option>
#               <option value="280">Foro de Itariri</option>
#               <option value="281">Foro de Itatiba</option>
#               <option value="282">Foro de Itatinga</option>
#               <option value="283">Foro de Itirapina</option>
#               <option value="286">Foro de Itu</option>
#               <option value="514">Foro de Itupeva</option>
#               <option value="288">Foro de Ituverava</option>
#               <option value="291">Foro de Jaboticabal</option>
#               <option value="292">Foro de Jacare�</option>
#               <option value="294">Foro de Jacupiranga</option>
#               <option value="296">Foro de Jaguari�na</option>
#               <option value="297">Foro de Jales</option>
#               <option value="299">Foro de Jandira</option>
#               <option value="300">Foro de Jardin�polis</option>
#               <option value="301">Foro de Jarinu</option>
#               <option value="302">Foro de Ja�</option>
#               <option value="306">Foro de Jos� Bonif�cio</option>
#               <option value="309">Foro de Jundia�</option>
#               <option value="311">Foro de Junqueir�polis</option>
#               <option value="312">Foro de Juqui�</option>
#               <option value="315">Foro de Laranjal Paulista</option>
#               <option value="318">Foro de Leme</option>
#               <option value="319">Foro de Len��is Paulista</option>
#               <option value="320">Foro de Limeira</option>
#               <option value="322">Foro de Lins</option>
#               <option value="323">Foro de Lorena</option>
#               <option value="681">Foro de Louveira</option>
#               <option value="326">Foro de Luc�lia</option>
#               <option value="333">Foro de Macatuba</option>
#               <option value="334">Foro de Macaubal</option>
#               <option value="337">Foro de Mairinque</option>
#               <option value="338">Foro de Mairipor�</option>
#               <option value="341">Foro de Maraca�</option>
#               <option value="344">Foro de Mar�lia</option>
#               <option value="346">Foro de Martin�polis</option>
#               <option value="347">Foro de Mat�o</option>
#               <option value="348">Foro de Mau�</option>
#               <option value="352">Foro de Miguel�polis</option>
#               <option value="355">Foro de Miracatu</option>
#               <option value="356">Foro de Mirand�polis</option>
#               <option value="357">Foro de Mirante do Paranapanema</option>
#               <option value="358">Foro de Mirassol</option>
#               <option value="360">Foro de Mococa</option>
#               <option value="361">Foro de Mogi das Cruzes</option>
#               <option value="362">Foro de Mogi Gua�u</option>
#               <option value="363">Foro de Mogi Mirim</option>
#               <option value="366">Foro de Mongagu�</option>
#               <option value="368">Foro de Monte Alto</option>
#               <option value="369">Foro de Monte Apraz�vel</option>
#               <option value="370">Foro de Monte Azul Paulista</option>
#               <option value="372">Foro de Monte Mor</option>
#               <option value="374">Foro de Morro Agudo</option>
#               <option value="695">Foro de Nazar� Paulista</option>
#               <option value="382">Foro de Neves Paulista</option>
#               <option value="383">Foro de Nhandeara</option>
#               <option value="390">Foro de Nova Granada</option>
#               <option value="394">Foro de Nova Odessa</option>
#               <option value="396">Foro de Novo Horizonte</option>
#               <option value="397">Foro de Nuporanga</option>
#               <option value="400">Foro de Ol�mpia</option>
#               <option value="404">Foro de Orl�ndia</option>
#               <option value="405">Foro de Osasco</option>
#               <option value="407">Foro de Osvaldo Cruz</option>
#               <option value="408">Foro de Ourinhos</option>
#               <option value="696">Foro de Ouroeste</option>
#               <option value="411">Foro de Pacaembu</option>
#               <option value="412">Foro de Palestina</option>
#               <option value="414">Foro de Palmeira D'Oeste</option>
#               <option value="415">Foro de Palmital</option>
#               <option value="416">Foro de Panorama</option>
#               <option value="417">Foro de Paragua�u Paulista</option>
#               <option value="418">Foro de Paraibuna</option>
#               <option value="420">Foro de Paranapanema</option>
#               <option value="424">Foro de Pariquera-A�u</option>
#               <option value="426">Foro de Patroc�nio Paulista</option>
#               <option value="428">Foro de Paul�nia</option>
#               <option value="430">Foro de Paulo de Faria</option>
#               <option value="431">Foro de Pederneiras</option>
#               <option value="434">Foro de Pedregulho</option>
#               <option value="435">Foro de Pedreira</option>
#               <option value="438">Foro de Pen�polis</option>
#               <option value="439">Foro de Pereira Barreto</option>
#               <option value="441">Foro de Peru�be</option>
#               <option value="443">Foro de Piedade</option>
#               <option value="444">Foro de Pilar do Sul</option>
#               <option value="445">Foro de Pindamonhangaba</option>
#               <option value="447">Foro de Pinhalzinho</option>
#               <option value="449">Foro de Piquete</option>
#               <option value="450">Foro de Piracaia</option>
#               <option value="451">Foro de Piracicaba</option>
#               <option value="452">Foro de Piraju</option>
#               <option value="453">Foro de Piraju�</option>
#               <option value="698">Foro de Pirangi</option>
#               <option value="456">Foro de Pirapozinho</option>
#               <option value="457">Foro de Pirassununga</option>
#               <option value="458">Foro de Piratininga</option>
#               <option value="459">Foro de Pitangueiras</option>
#               <option value="462">Foro de Po�</option>
#               <option value="464">Foro de Pomp�ia</option>
#               <option value="466">Foro de Pontal</option>
#               <option value="470">Foro de Porangaba</option>
#               <option value="471">Foro de Porto Feliz</option>
#               <option value="472">Foro de Porto Ferreira</option>
#               <option value="474">Foro de Potirendaba</option>
#               <option value="477">Foro de Praia Grande</option>
#               <option value="480">Foro de Presidente Bernardes</option>
#               <option value="481">Foro de Presidente Epit�cio</option>
#               <option value="482">Foro de Presidente Prudente</option>
#               <option value="483">Foro de Presidente Venceslau</option>
#               <option value="484">Foro de Promiss�o</option>
#               <option value="486">Foro de Quat�</option>
#               <option value="488">Foro de Queluz</option>
#               <option value="491">Foro de Rancharia</option>
#               <option value="493">Foro de Regente Feij�</option>
#               <option value="495">Foro de Registro</option>
#               <option value="498">Foro de Ribeir�o Bonito</option>
#               <option value="505">Foro de Ribeir�o Pires</option>
#               <option value="506">Foro de Ribeir�o Preto</option>
#               <option value="510">Foro de Rio Claro</option>
#               <option value="511">Foro de Rio das Pedras</option>
#               <option value="512">Foro de Rio Grande da Serra</option>
#               <option value="515">Foro de Rosana</option>
#               <option value="516">Foro de Roseira</option>
#               <option value="523">Foro de Sales�polis</option>
#               <option value="526">Foro de Salto</option>
#               <option value="699">Foro de Salto de Pirapora</option>
#               <option value="531">Foro de Santa Ad�lia</option>
#               <option value="533">Foro de Santa B�rbara D'Oeste</option>
#               <option value="534">Foro de Santa Branca</option>
#               <option value="538">Foro de Santa Cruz das Palmeiras</option>
#               <option value="539">Foro de Santa Cruz do Rio Pardo</option>
#               <option value="541">Foro de Santa F� do Sul</option>
#               <option value="543">Foro de Santa Isabel</option>
#               <option value="547">Foro de Santa Rita do Passa Quatro</option>
#               <option value="549">Foro de Santa Rosa de Viterbo</option>
#               <option value="529">Foro de Santana de Parna�ba</option>
#               <option value="553">Foro de Santo Anast�cio</option>
#               <option value="554">Foro de Santo Andr�</option>
#               <option value="562">Foro de Santos</option>
#               <option value="563">Foro de S�o Bento do Sapuca�</option>
#               <option value="564">Foro de S�o Bernardo do Campo</option>
#               <option value="565">Foro de S�o Caetano do Sul</option>
#               <option value="566">Foro de S�o Carlos</option>
#               <option value="568">Foro de S�o Jo�o da Boa Vista</option>
#               <option value="572">Foro de S�o Joaquim da Barra</option>
#               <option value="575">Foro de S�o Jos� do Rio Pardo</option>
#               <option value="576">Foro de S�o Jos� do Rio Preto</option>
#               <option value="577">Foro de S�o Jos� dos Campos</option>
#               <option value="579">Foro de S�o Luiz do Paraitinga</option>
#               <option value="581">Foro de S�o Manuel</option>
#               <option value="582">Foro de S�o Miguel Arcanjo</option>
#               <option value="584">Foro de S�o Pedro</option>
#               <option value="586">Foro de S�o Roque</option>
#               <option value="587">Foro de S�o Sebasti�o</option>
#               <option value="588">Foro de S�o Sebasti�o da Grama</option>
#               <option value="589">Foro de S�o Sim�o</option>
#               <option value="590">Foro de S�o Vicente</option>
#               <option value="595">Foro de Serra Negra</option>
#               <option value="596">Foro de Serrana</option>
#               <option value="597">Foro de Sert�ozinho</option>
#               <option value="601">Foro de Socorro</option>
#               <option value="602">Foro de Sorocaba</option>
#               <option value="604">Foro de Sumar�</option>
#               <option value="606">Foro de Suzano</option>
#               <option value="607">Foro de Tabapu�</option>
#               <option value="609">Foro de Tabo�o da Serra</option>
#               <option value="614">Foro de Tamba�</option>
#               <option value="615">Foro de Tanabi</option>
#               <option value="619">Foro de Taquaritinga</option>
#               <option value="620">Foro de Taquarituba</option>
#               <option value="624">Foro de Tatu�</option>
#               <option value="625">Foro de Taubat�</option>
#               <option value="627">Foro de Teodoro Sampaio</option>
#               <option value="629">Foro de Tiet�</option>
#               <option value="634">Foro de Trememb�</option>
#               <option value="637">Foro de Tup�</option>
#               <option value="638">Foro de Tupi Paulista</option>
#               <option value="642">Foro de Ubatuba</option>
#               <option value="646">Foro de Ur�nia</option>
#               <option value="648">Foro de Urup�s</option>
#               <option value="650">Foro de Valinhos</option>
#               <option value="651">Foro de Valpara�so</option>
#               <option value="653">Foro de Vargem Grande do Sul</option>
#               <option value="654">Foro de Vargem Grande Paulista</option>
#               <option value="655">Foro de V�rzea Paulista</option>
#               <option value="659">Foro de Vinhedo</option>
#               <option value="660">Foro de Viradouro</option>
#               <option value="663">Foro de Votorantim</option>
#               <option value="664">Foro de Votuporanga</option>
#               <option value="12">Foro Distrital de Parelheiros</option>
#               <option value="15">Foro Especial da Inf�ncia e Juventude</option>
#               <option value="635">Foro Plant�o - 00� CJ - Capital</option>
#               <option value="536">Foro Plant�o - 01� CJ - Santos</option>
#               <option value="537">Foro Plant�o - 02� CJ - S�o Be. Campo</option>
#               <option value="540">Foro Plant�o - 03� CJ - Santo Andr�</option>
#               <option value="542">Foro Plant�o - 04� CJ - Osasco</option>
#               <option value="544">Foro Plant�o - 05� CJ - Jundia�</option>
#               <option value="545">Foro Plant�o - 06� CJ - Brag. Paulista</option>
#               <option value="546">Foro Plant�o - 07� CJ - Mogi Mirim</option>
#               <option value="548">Foro Plant�o - 08� CJ - Campinas</option>
#               <option value="550">Foro Plant�o - 09� CJ - Rio Claro</option>
#               <option value="551">Foro Plant�o - 10� CJ - Limeira</option>
#               <option value="552">Foro Plant�o - 11� CJ - Pirassununga</option>
#               <option value="555">Foro Plant�o - 12� CJ - S�o Carlos</option>
#               <option value="556">Foro Plant�o - 13� CJ - Araraquara</option>
#               <option value="557">Foro Plant�o - 14� CJ - Barretos</option>
#               <option value="558">Foro Plant�o - 15� CJ - Catanduva</option>
#               <option value="559">Foro Plant�o - 16� CJ - S. J. Rio Preto</option>
#               <option value="560">Foro Plant�o - 17� CJ - Votuporanga</option>
#               <option value="561">Foro Plant�o - 18� CJ - Fernand�polis</option>
#               <option value="567">Foro Plant�o - 19� CJ - Sorocaba</option>
#               <option value="569">Foro Plant�o - 20� CJ - Itu</option>
#               <option value="570">Foro Plant�o - 21� CJ - Registro</option>
#               <option value="571">Foro Plant�o - 22� CJ - Itapetininga</option>
#               <option value="573">Foro Plant�o - 23� CJ - Botucatu</option>
#               <option value="574">Foro Plant�o - 24� CJ - Avar�</option>
#               <option value="578">Foro Plant�o - 25� CJ - Ourinhos</option>
#               <option value="580">Foro Plant�o - 26� CJ - Assis</option>
#               <option value="583">Foro Plant�o - 27� CJ - Pre. Prudente</option>
#               <option value="585">Foro Plant�o - 28� CJ - Pre. Venceslau</option>
#               <option value="591">Foro Plant�o - 29� CJ - Dracena</option>
#               <option value="592">Foro Plant�o - 30� CJ - Tup�</option>
#               <option value="593">Foro Plant�o - 31� CJ - Mar�lia</option>
#               <option value="594">Foro Plant�o - 32� CJ - Bauru</option>
#               <option value="598">Foro Plant�o - 33� CJ - Ja�</option>
#               <option value="599">Foro Plant�o - 34� CJ - Piracicaba</option>
#               <option value="600">Foro Plant�o - 35� CJ - Lins</option>
#               <option value="603">Foro Plant�o - 36� CJ - Ara�atuba</option>
#               <option value="605">Foro Plant�o - 37� CJ - Andradina</option>
#               <option value="608">Foro Plant�o - 38� CJ - Franca</option>
#               <option value="610">Foro Plant�o - 39� CJ - Batatais</option>
#               <option value="611">Foro Plant�o - 40� CJ - Ituverava</option>
#               <option value="530">Foro Plant�o - 41� CJ - Ribeir�o Preto</option>
#               <option value="612">Foro Plant�o - 42� CJ - Jaboticabal</option>
#               <option value="613">Foro Plant�o - 43� CJ - Casa Branca</option>
#               <option value="535">Foro Plant�o - 44� CJ - Guarulhos</option>
#               <option value="616">Foro Plant�o - 45� CJ - Mogi das Cruzes</option>
#               <option value="617">Foro Plant�o - 46� CJ - S. J. dos Campos</option>
#               <option value="618">Foro Plant�o - 47� CJ - Taubat�</option>
#               <option value="621">Foro Plant�o - 48� CJ - Guaratinguet�</option>
#               <option value="622">Foro Plant�o - 49� CJ - Itapeva</option>
#               <option value="623">Foro Plant�o - 50� CJ - S. J. Boa Vista</option>
#               <option value="626">Foro Plant�o - 51� CJ - Caraguatatuba</option>
#               <option value="628">Foro Plant�o - 52� CJ - Itapec. da Serra</option>
#               <option value="630">Foro Plant�o - 53� CJ - Americana</option>
#               <option value="631">Foro Plant�o - 54� CJ - Amparo</option>
#               <option value="632">Foro Plant�o - 55� CJ - Jales</option>
#               <option value="633">Foro Plant�o - 56� CJ - Itanha�m</option>
#               <option value="84">Foro Regional de Vila Mimosa</option>
#               <option value="1">Foro Regional I - Santana</option>
#               <option value="2">Foro Regional II - Santo Amaro</option>
#               <option value="3">Foro Regional III - Jabaquara</option>
#               <option value="4">Foro Regional IV - Lapa</option>
#               <option value="9">Foro Regional IX - Vila Prudente</option>
#               <option value="5">Foro Regional V - S�o Miguel Paulista</option>
#               <option value="6">Foro Regional VI - Penha de Fran�a</option>
#               <option value="7">Foro Regional VII - Itaquera</option>
#               <option value="8">Foro Regional VIII - Tatuap�</option>
#               <option value="10">Foro Regional X - Ipiranga</option>
#               <option value="11">Foro Regional XI - Pinheiros</option>
#               <option value="20">Foro Regional XII - Nossa Senhora do �</option>
#               <option value="704">Foro Regional XV - Butant�</option>
#               <option value="996">Presidente Prudente/DEECRIM UR5</option>
#               <option value="496">Ribeir�o Preto/DEECRIM UR6</option>
#               <option value="158">Santos/DEECRIM UR7</option>
#               <option value="154">S�o Jos� do Rio Preto/DEECRIM UR8</option>
#               <option value="520">S�o Jos� dos Campos/DEECRIM UR9</option>
#               <option value="41">S�o Paulo/DEECRIM UR1</option>
#               <option value="21">Setor de Cartas Precat�rias C�veis - Cap</option>
#               <option value="521">Sorocaba/DEECRIM UR10</option>
#             </select>
#           </td>
#         </tr>
#         <tr class="">
#           <td id="" width="150" valign="">
#             <label for="cbPesquisa" class="" style="text-align:right;font-weight:bold;;">Pesquisar por:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <select name="cbPesquisa" id="cbPesquisa">
#                       <option value="NUMPROC" selected="selected">N�mero do Processo</option>
#                       <option value="NMPARTE">Nome da parte</option>
#                       <option value="DOCPARTE">Documento da Parte</option>
#                       <option value="NMADVOGADO">Nome do Advogado</option>
#                       <option value="NUMOAB">OAB</option>
#                       <option value="PRECATORIA">N� da Carta Precat�ria na Origem</option>
#                       <option value="DOCDELEG">N� do Documento na Delegacia</option>
#                       <option value="NUMCDA">CDA</option>
#                     </select>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <script>$.saj.numeroProcesso.desabilitarNumeroOculto = false;</script>  
#         <script type="text/javascript">
#           (function($){
#             $(function() {
#                 if($.browser.msie){
#                     $('.grupoRadio').find('input:radio:visible:enabled').attr('aria-required','');
#                     return;
#                 }
#                 $('.grupoRadio').attr('aria-required','').attr('required','');
#             });
#           })(jQuery);
#         </script>
#         <tr class="">
#           <td id="" width="150" valign="">
#           </td>
#           <td valign="">
#             <table cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <fieldset class="grupoRadio" style="margin:0;padding:0;border:none" aria-required="" required="required">
#                       <legend style="display:block;font-size:0;">
#                         Tipo do n�mero
#                       </legend>
#                       <input type="radio" name="dadosConsulta.tipoNuProcesso" value="UNIFICADO" checked="checked" id="radioNumeroUnificado"><label for="radioNumeroUnificado">Unificado</label>
#                       <input type="radio" name="dadosConsulta.tipoNuProcesso" value="SAJ" id="radioNumeroAntigo"><label for="radioNumeroAntigo">Outros</label>
#                     </fieldset>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="NUMPROC" class="" style="display: table-row;">
#           <td id="" width="150" valign="">
#             <label for="" class="" style="text-align:right;font-weight:bold;;">N�mero do Processo:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <span id="linhaProcessoUnificado">
#                       <input type="text" name="numeroDigitoAnoUnificado" maxlength="25" size="20" value="0162771-09.2006" formattype="TEXT" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" id="numeroDigitoAnoUnificado" aria-label="N�mero do processo. Informe os treze primeiros d�gitos do n�mero do processo." aria-required="true" required="required" class="inputValido">
#                       <input type="text" id="JTRNumeroUnificado" size="3" disabled="disabled" value="8.26" aria-required="true" required="required">
#                       <input type="text" name="foroNumeroUnificado" maxlength="4" size="3" value="0100" formattype="TEXT" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" id="foroNumeroUnificado" aria-label="Informe os quatro �ltimos d�gitos do n�mero do processo." aria-required="true" required="required" class="inputValido">
#                       <input type="hidden" name="dadosConsulta.valorConsultaNuUnificado" value="0162771-09.2006.8.26.0100" id="nuProcessoUnificadoFormatado">
#                       <script>
#                         (function($){
#                             $(function() {
#                                 var saj = $.saj;
#                                 var mascaraNumeroUnificado = saj.mascaraNumeroUnificado;
#                                 var $campoNumeroDigitoAnoUnificado = $('#numeroDigitoAnoUnificado');
#                                 var $campoForoNumeroUnificado = $('#foroNumeroUnificado');
#                                 var $campoJTRNumeroUnificado = $('#JTRNumeroUnificado');
#                                 saj.configurarMascaraInput($campoForoNumeroUnificado, '0000');
#                                 saj.configurarMascaraInput($campoNumeroDigitoAnoUnificado, mascaraNumeroUnificado, $campoForoNumeroUnificado,{
#                                     afterPaste: afterPaste          
#                                 });
#                                 saj.bindCamposNumeroProcesso('');
                                
#                                     var conteudoToolTip = '';
#                                     if(!conteudoToolTip){
#                                         conteudoToolTip = '<div style="width: 500px;" ><div><b>N�mero de Processo Unificado</b></div><div>O sistema disponibiliza facilidades no preenchimento do n�mero unificado, seu formato � NNNNNNN-DD.AAAA.J.TR.OOOO:</div><div style="padding-top: 3px;"><b>NNNNNNN</b>: Caso o n�mero possua zeros � esquerda o sistema preenche-os automaticamente, basta informar o n�mero e o d�gito "-" ou ".". Exemplo: ao informar "310-" o sistema ir� preencher "0000310-".</div><div><b>DD</b>     : Deve ser preenchido pelo usu�rio.</div><div><b>AAAA</b>   : Ao informar dois d�gitos para o ano o sistema completa o mesmo, basta pressionar a tecla Tab. Exemplo: ao informar "08" e "Tab" o sistema ir� preencher "2008".</div><div><b>J.TR</b>    : S�o n�meros fixos preenchidos pelo sistema.Exemplo: 8.99.</div><div><b>OOOO</b>   : Caso o n�mero possua zeros � esquerda o sistema preenche-os automaticamente, basta informar o n�mero pressionar a tecla Tab. Exemplo: ao informar "10" e "Tab" o sistema ir� preencher "0010".</div></div>';
#                                     }
#                                     $('#numeroDigitoAnoUnificado, #foroNumeroUnificado').registrarTooltip({
#                                         conteudoTooltip: conteudoToolTip,
#                                         localImagensTooltip: '/cpopg/imagens/saj',
#                                         offsetHorizontalExtra: '',
#                                         offsetVertficalExtra: '',
#                                         urlConteudoTooltip: '',
#                                         posicaoTooltip: '',
#                                         objReferenciaPosicaoTooltip:$campoForoNumeroUnificado
#                                     });
                                
#                             });     
                            
#                             var afterPaste = function(textoOriginal) {
#                                 var texto = getDigits(textoOriginal);
#                                 if (!texto || texto.length < 16) {
#                                     return;
#                                 }
                                
#                                 var jtr = $('#JTRNumeroUnificado').val();
#                                 jtr = getDigits(jtr);
#                                 var jtrDigitado = texto.substring(13,16);
#                                 var foroUnificado;
#                                 //pega os 4 ultimos digitos, e caso o jtr colado for o mesmo, coloca os 4 no campo de foro unificado
#                                 //se o jtr for diferente nao coloca nada no campo de foro unificado
#                                 if (jtr == jtrDigitado) {
#                                     foroUnificado = texto.substring(16,texto.length);
#                                 } else {
#                                     foroUnificado = '';
#                                 }
                                
#                                 var numeroDigitoAno = texto.substring(0,13);
#                                 var $numeroDigitoAno = $('#numeroDigitoAnoUnificado');
#                                 $numeroDigitoAno.val(numeroDigitoAno);
                                
#                                 var $foroNumeroUnificado = $('#foroNumeroUnificado');
#                                 $foroNumeroUnificado.val(foroUnificado);
#                                 $foroNumeroUnificado.trigger('blur');
#                                 $foroNumeroUnificado.focus();
#                             };
                            
#                             var getDigits = function(texto) {
#                                 return texto.replace(/[^0-9]/g, '');
#                             };
#                             $('#numeroDigitoAnoUnificado').attr('aria-label','N�mero do processo. Informe os treze primeiros d�gitos do n�mero do processo.');
#                             $('#foroNumeroUnificado').attr('aria-label','Informe os quatro �ltimos d�gitos do n�mero do processo.');
#                         })(jQuery);
                        
#                       </script>
#                     </span>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'nuProcessoAntigoFormatado';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="linhaProcessoAntigo" style="display: none;">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="25" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" style="" class="spwCampoTexto" id="nuProcessoAntigoFormatado" title="" alt="" aria-required="true" required="required" aria-label="N�mero do processo">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="NMPARTE" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">Nome da parte:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_NMPARTE" style="display:block;font-size:0;">Nome da parte</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_NMPARTE';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled spwCampoTexto" id="campo_NMPARTE" title="" alt="" aria-required="true" required="required">
#                     </span>
#                     <span id="">
#                     <label for="id_tag.checkbox.nomecompleto.rotulo">
#                     <input type="checkbox" name="chNmCompleto" value="true" style="vertical-align:middle;" id="id_tag.checkbox.nomecompleto.rotulo" disabled="disabled">
#                     Pesquisar por nome completo
#                     </label>
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="DOCPARTE" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">Documento da Parte:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_DOCPARTE" style="display:block;font-size:0;">Documento da Parte</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_DOCPARTE';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled " id="campo_DOCPARTE" title="" alt="" aria-required="true" required="required">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="NMADVOGADO" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">Nome do Advogado:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_NMADVOGADO" style="display:block;font-size:0;">Nome do Advogado</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_NMADVOGADO';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled spwCampoTexto" id="campo_NMADVOGADO" title="" alt="" aria-required="true" required="required">
#                     </span>
#                     <span id="">
#                     <label for="id_tag.checkbox.nomecompleto.rotulo">
#                     <input type="checkbox" name="chNmCompleto" value="true" style="vertical-align:middle;" id="id_tag.checkbox.nomecompleto.rotulo" disabled="disabled">
#                     Pesquisar por nome completo
#                     </label>
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="NUMOAB" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">OAB:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_NUMOAB" style="display:block;font-size:0;">OAB</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_NUMOAB';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled " id="campo_NUMOAB" title="" alt="" aria-required="true" required="required">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="PRECATORIA" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">N� da precat�ria:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_PRECATORIA" style="display:block;font-size:0;">N� da precat�ria</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_PRECATORIA';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled " id="campo_PRECATORIA" title="" alt="" aria-required="true" required="required">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="DOCDELEG" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">N� na delegacia:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_DOCDELEG" style="display:block;font-size:0;">N� na delegacia</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_DOCDELEG';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled " id="campo_DOCDELEG" title="" alt="" aria-required="true" required="required">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <tr id="NUMCDA" class="" style="display: none;">
#           <td id="" width="150" valign="">
#             <label for="chNmCompleto" class="" style="text-align:right;font-weight:bold;;">CDA:</label>
#           </td>
#           <td valign="">
#             <table cellpadding="0" cellspacing="0" border="0" width="">
#               <tbody>
#                 <tr>
#                   <td>
#                     <label for="campo_NUMCDA" style="display:block;font-size:0;">CDA</label>
#                     <style>
#                       /* remove borda vermelha de elementos required */
#                       input:invalid {
#                       box-shadow:none;
#                       }
#                     </style>
#                     <script type="text/javascript">
#                       (function($){
#                         $(function() {
#                             var saj = $.saj;
#                             var id = 'campo_NUMCDA';
#                             var idObjetoReferencia = '#' + id;
#                             if ('' !== '' && !false) {
#                                 $(idObjetoReferencia).registrarTooltip({
#                                     conteudoTooltip: '',
#                                     posicaoTooltip: 'direita',
#                                     objReferenciaPosicaoTooltip:$(idObjetoReferencia)
#                                 });
#                             }
#                             //remove comportamento de exibir mensagem de erro t�pica do html5
#                             $('form:visible').attr('novalidate','');
#                             if (''){
#                                 $(idObjetoReferencia).attr('aria-required','true').attr('required','');
#                             }
#                         });     
#                       })(jQuery);
                      
#                     </script>
#                     <span id="">
#                     <input type="text" name="dadosConsulta.valorConsulta" size="" value="" formattype="TEXT" formato="100" obrigatorio="" rotulo="" onkeypress="CT_KPS(this, event);" onblur="CT_BLR(this);" onkeydown="CT_KDN(this, event);" onmousemove="CT_MMOV(this, event);" onmouseout="CT_MOUT(this, event);" onmouseover="CT_MOV(this, event);" onfocus="C_OFC(this, event);" disabled="disabled" style="" class="spwCampoTexto disabled " id="campo_NUMCDA" title="" alt="" aria-required="true" required="required">
#                     </span>
#                   </td>
#                 </tr>
#               </tbody>
#             </table>
#           </td>
#         </tr>
#         <input id="uuidCaptcha" type="hidden" name="uuidCaptcha" value="sajcaptcha_92cf15e871d14665973588b8ccea3251">
#       </tbody>
#     </table>
#   </div>
#   <table id="" class="secaoBotoesBody" width="100%" style="" cellpadding="2" cellspacing="0" border="0">
#     <tbody>
#       <tr>
#         <td width="150">&nbsp;</td>
#         <td align="">
#           <input type="submit" name="pbEnviar" value="Pesquisar" onclick="" onmouseover="B_mOver(this);" onmouseout="B_mOut(this);" class="spwBotaoDefault " id="pbEnviar">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </form>
# <script type="text/javascript">
#   (function ($) {
#       $(function () {
#           var captcha = $.saj.getUrlParameter('uuidCaptcha');
  
#           if(!captcha){
#               return;
#           }
#           var $processoPrinc = $('.processoPrinc');
#           $processoPrinc.attr('href', $processoPrinc.attr('href') + '&uuidCaptcha=' + captcha);
  
#           var $processoPaiApenso = $('.processoPaiApenso');
#           $processoPaiApenso.attr('href', $processoPaiApenso.attr('href') + '&uuidCaptcha=' + captcha);
  
#       })
#   })(jQuery);
# </script>
# <!-- pasta digital -->
# <div class="">
#   <br>
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Dados do processo
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
#   <br>
#   <table id="" class="secaoFormBody" width="100%" style="" cellpadding="2" cellspacing="0" border="0">
#     <tbody>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="dadosFmt.numero" class="labelClass" style="text-align:right;font-weight:bold;;">Processo:</label>
#         </td>
#         <td valign="">
#           <table cellpadding="0" cellspacing="0" border="0" width="">
#             <tbody>
#               <tr>
#                 <td>
#                   <!-- Atributos -->
#                   <span class="">
#                   0162771-09.2006.8.26.0100
#                   </span>
#                   <span class="">
#                   (583.00.2006.162771)
#                   </span>
#                   <span style="color: red; padding-left: 6px;">Suspenso</span>
#                 </td>
#               </tr>
#             </tbody>
#           </table>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="numeroProcessoSeDependente" class="labelClass" style="text-align:right;font-weight:bold;;">Classe:</label>
#         </td>
#         <td valign="">
#           <table cellpadding="0" cellspacing="0" border="0" width="">
#             <tbody>
#               <tr>
#                 <td>
#                   <span id="">
#                   <span id="" class="">Reintegra��o / Manuten��o de Posse</span>
#                   </span>
#                   &nbsp;
#                   <span id="">
#                   <span id="" class="">&nbsp;</span>
#                   </span>
#                 </td>
#               </tr>
#             </tbody>
#           </table>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#         </td>
#         <td valign="">
#           <table cellpadding="0" cellspacing="0" border="0" width="">
#             <tbody>
#               <tr>
#                 <td>
#                   <span class="labelClass">�rea:</span> C�vel 
#                 </td>
#               </tr>
#             </tbody>
#           </table>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="" class="labelClass" style="text-align:right;font-weight:bold;;">Local F�sico:</label>
#         </td>
#         <td valign="">
#           <span id="" class="">27/10/2015 00:00 - Servi�o de M�quina - v. C.S.</span>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="" class="labelClass" style="text-align:right;font-weight:bold;;">Distribui��o:</label>
#         </td>
#         <td valign="">
#           <span id="" class="">09/06/2006 �s 16:46 - Livre</span>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#         </td>
#         <td valign="">
#           <span id="" class="">15� Vara C�vel - Foro Central C�vel</span>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="" class="labelClass" style="text-align:right;font-weight:bold;;">Controle:</label>
#         </td>
#         <td valign="">
#           <span id="" class="">2006/000870</span>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="" class="labelClass" style="text-align:right;font-weight:bold;;">Juiz:</label>
#         </td>
#         <td valign="">
#           <span id="" class="">Celina Dietrich Trigueiros Teixeira Pinto</span>
#         </td>
#       </tr>
#       <tr class="">
#         <td id="" width="150" valign="">
#           <label for="" class="labelClass" style="text-align:right;font-weight:bold;;">Valor da a��o:</label>
#         </td>
#         <td valign="">
#           <span id="" class="">R$         6.650,00</span>
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Partes do processo
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <div id="divLinksTituloBlocoPartes">
#   <input id="mensagemNaoExibidapartes" type="hidden" value="Exibindo todas as partes.">  
#   <input id="linkNaoExibidopartes" type="hidden" value="<span id=&quot;setasDireitapartes&quot; class=&quot;setasDireita&quot;>&amp;gt;&amp;gt;</span>Exibir somente as partes principais.">  
#   <span id="mensagensExibindopartes" class="mensagemExibindo">Exibindo Somente as principais partes.</span> &nbsp; <a id="linkpartes" href="javascript:" class="linkNaoSelecionado"><span id="setasDireitapartes" class="setasDireita">&gt;&gt;</span>Exibir todas as partes.</a>
#   <script>
#     $(function() {
#         var controlarLink = function() {
#             var $linkNaoExibido = $('input#linkNaoExibidopartes');
#             var conteudoLinkNaoApresentado = $linkNaoExibido.val();
#             var $link = $('a#linkpartes');
            
#             $linkNaoExibido.val($link.html());
#             $link.html(conteudoLinkNaoApresentado);
#         };
    
#         var controlarMensagem = function() {
#             var $mensagemNaoExibida = $('input#mensagemNaoExibidapartes');
#             var $spanMensagem = $('span#mensagensExibindopartes');
#             var mensagemExibida = $spanMensagem.html();
#             var mensagemNaoExibida = $mensagemNaoExibida.val();
            
#             $spanMensagem.html(mensagemNaoExibida);
#             $mensagemNaoExibida.val(mensagemExibida);
#         };
    
#         var controlarMensagemLink = function() {
#             controlarMensagem();
#             controlarLink();
#         };
        
#         var esconderElementosExtrasMostrarDefault = function() {
#             $('#tableTodasPartes').hide();
#             $('#tablePartesPrincipais').show();
#         };
    
#         var mostrarElementosExtrasEsconderDefault = function() {
#             $('#tablePartesPrincipais').hide();
#             $('#tableTodasPartes').show();
#         };
    
#         var initPagina = function() {
#             var setasDireita = '<span id="setasDireitapartes" class="setasDireita">&gt;&gt;</span>';
#             var $linkEscondido = $('input#linkNaoExibidopartes');
#             $linkEscondido.val(setasDireita+$linkEscondido.val());
#         };
    
#         var bindLink = function() {
#             var $link = $('a#linkpartes');
#             $link.funcToggle('click', mostrarElementosExtrasEsconderDefault, esconderElementosExtrasMostrarDefault);
#             $link.bind('click', controlarMensagemLink);
#         };
    
#         initPagina();
#         bindLink();
#         esconderElementosExtrasMostrarDefault();
#     });
#   </script>
# </div>
# <!--  cabecalho da tabela de lista (partes) -->
# <!--  dados da lista partes principais (partes) -->
# <table id="tablePartesPrincipais" style="margin-left:15px; margin-top:1px;" align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
#   <tbody>
#     <tr class="fundoClaro">
#       <td valign="top" align="right" width="141" style="padding-bottom: 5px">
#         <span class="mensagemExibindo">Reqte:&nbsp;</span>
#       </td>
#       <td width="*" align="left" style="padding-bottom: 5px">
#         Companhia Metropolitana de Habita�ao de Sao Paulo - Cohab Sp
#         <br>
#         <span class="mensagemExibindo">Advogada:&nbsp;</span>
#         Alessandra Devulsky da Silva Tisescu&nbsp;
#         <br>
#         <span class="mensagemExibindo">Advogada:&nbsp;</span>
#         Beatriz Helena Theophilo&nbsp;
#       </td>
#     </tr>
#     <tr class="fundoClaro">
#       <td valign="top" align="right" width="141" style="padding-bottom: 5px">
#         <span class="mensagemExibindo">Reqdo:&nbsp;</span>
#       </td>
#       <td width="*" align="left" style="padding-bottom: 5px">
#         Creso Ronaldo Domingues Vieira
#         <br>
#         <span class="mensagemExibindo">Advogado:&nbsp;</span>
#         Carlos Roberto Domingues Vieira&nbsp;
#       </td>
#     </tr>
#   </tbody>
# </table>
# <!--  dados da lista todas as partes (partes) -->
# <table id="tableTodasPartes" style="margin-left:15px; margin-top:1px; display: none; " align="center" width="98%" border="0" cellspacing="0" cellpadding="0">
#   <tbody>
#     <tr class="fundoClaro">
#       <td valign="top" align="right" width="141" style="padding-bottom: 5px">
#         <span class="mensagemExibindo">Reqte:&nbsp;</span>
#       </td>
#       <td width="*" align="left" style="padding-bottom: 5px">
#         Companhia Metropolitana de Habita�ao de Sao Paulo - Cohab Sp
#         <br>
#         <span class="mensagemExibindo">Advogada:&nbsp;</span>
#         Alessandra Devulsky da Silva Tisescu&nbsp;
#         <br>
#         <span class="mensagemExibindo">Advogada:&nbsp;</span>
#         Beatriz Helena Theophilo&nbsp;
#       </td>
#     </tr>
#     <tr class="fundoClaro">
#       <td valign="top" align="right" width="141" style="padding-bottom: 5px">
#         <span class="mensagemExibindo">Reqdo:&nbsp;</span>
#       </td>
#       <td width="*" align="left" style="padding-bottom: 5px">
#         Creso Ronaldo Domingues Vieira
#         <br>
#         <span class="mensagemExibindo">Advogado:&nbsp;</span>
#         Carlos Roberto Domingues Vieira&nbsp;
#       </td>
#     </tr>
#     <tr class="fundoClaro">
#       <td valign="top" align="right" width="141" style="padding-bottom: 5px">
#         <span class="mensagemExibindo">Reqdo:&nbsp;</span>
#       </td>
#       <td width="*" align="left" style="padding-bottom: 5px">
#         Silvia Regina Resquim Vieira
#         <br>
#         <span class="mensagemExibindo">Advogado:&nbsp;</span>
#         Carlos Roberto Domingues Vieira&nbsp;
#       </td>
#     </tr>
#   </tbody>
# </table>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Movimenta��es
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <div id="divLinksTituloBlocoMovimentacoes">
#   <input id="mensagemNaoExibidamovimentacoes" type="hidden" value="Exibindo todas as movimenta��es.">  
#   <input id="linkNaoExibidomovimentacoes" type="hidden" value="<span id=&quot;setasDireitamovimentacoes&quot; class=&quot;setasDireita&quot;>&amp;gt;&amp;gt;</span>Listar somente as 5 �ltimas.">  
#   <span id="mensagensExibindomovimentacoes" class="mensagemExibindo">Exibindo 5 �ltimas.</span> &nbsp; <a id="linkmovimentacoes" href="javascript:" class="linkNaoSelecionado"><span id="setasDireitamovimentacoes" class="setasDireita">&gt;&gt;</span>Listar todas as movimenta��es.</a>
#   <script>
#     $(function() {
#         var controlarLink = function() {
#             var $linkNaoExibido = $('input#linkNaoExibidomovimentacoes');
#             var conteudoLinkNaoApresentado = $linkNaoExibido.val();
#             var $link = $('a#linkmovimentacoes');
            
#             $linkNaoExibido.val($link.html());
#             $link.html(conteudoLinkNaoApresentado);
#         };
    
#         var controlarMensagem = function() {
#             var $mensagemNaoExibida = $('input#mensagemNaoExibidamovimentacoes');
#             var $spanMensagem = $('span#mensagensExibindomovimentacoes');
#             var mensagemExibida = $spanMensagem.html();
#             var mensagemNaoExibida = $mensagemNaoExibida.val();
            
#             $spanMensagem.html(mensagemNaoExibida);
#             $mensagemNaoExibida.val(mensagemExibida);
#         };
    
#         var controlarMensagemLink = function() {
#             controlarMensagem();
#             controlarLink();
#         };
        
#         var esconderElementosExtrasMostrarDefault = function() {
#             $('#tabelaTodasMovimentacoes').hide();
#             $('#tabelaUltimasMovimentacoes').show();
#         };
    
#         var mostrarElementosExtrasEsconderDefault = function() {
#             $('#tabelaUltimasMovimentacoes').hide();
#             $('#tabelaTodasMovimentacoes').show();
#         };
    
#         var initPagina = function() {
#             var setasDireita = '<span id="setasDireitamovimentacoes" class="setasDireita">&gt;&gt;</span>';
#             var $linkEscondido = $('input#linkNaoExibidomovimentacoes');
#             $linkEscondido.val(setasDireita+$linkEscondido.val());
#         };
    
#         var bindLink = function() {
#             var $link = $('a#linkmovimentacoes');
#             $link.funcToggle('click', mostrarElementosExtrasEsconderDefault, esconderElementosExtrasMostrarDefault);
#             $link.bind('click', controlarMensagemLink);
#         };
    
#         initPagina();
#         bindLink();
#         esconderElementosExtrasMostrarDefault();
#     });
#   </script>
# </div>
# <table style="margin-left:15px; margin-top:1px;" align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
#   <thead>
#     <tr>
#       <th width="120" class="label" style="vertical-align: bottom">Data</th>
#       <th valign="middle" width="20" aria-hidden="true">&nbsp;</th>
#       <th valign="middle" class="label">Movimento</th>
#     </tr>
#     <tr class="fundoEscuro" height="2" aria-hidden="true">
#       <td></td>
#       <td></td>
#       <td></td>
#     </tr>
#   </thead>
#   <tbody id="tabelaUltimasMovimentacoes">
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/07/2017
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Arquivado Provisoriamente
#         <br> 
#         <span style="font-style: italic;">
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         23/04/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         In�cio da Execu��o Juntado
#         <br> 
#         <span style="font-style: italic;">
#         Seq.: 01 - Cumprimento de senten�a
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         11/04/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Certid�o de Publica��o Expedida
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o :0072/2014
#         Data da Disponibiliza��o: 11/04/2014
#         Data da Publica��o: 14/04/2014
#         N�mero do Di�rio: 1631
#         P�gina: 276/283
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/02/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Certid�o de Publica��o Expedida
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o :0036/2014
#         Data da Disponibiliza��o: 20/02/2014
#         Data da Publica��o: 21/02/2014
#         N�mero do Di�rio: 1597
#         P�gina: 264/284
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/02/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remetido ao DJE
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o: 0036/2014
#         Teor do ato: Fls. 368/372: Diante da recomenda��o da Corregedoria Geral da Justi�a do Estado de S�o Paulo com refer�ncia ao Of�cio n� 1795/2013 do Minist�rio P�blico do Estado de S�o Paulo, encaminhem-se os autos ao Minist�rio P�blico, � Primeira Promotoria de Justi�a de Habita��o e Urbanismo da Capital para eventual interesse em manifestar-se nos autos, com urg�ncia. Int
#         Advogados(s): Carlos Roberto Domingues Vieira (OAB 109410/SP), Alessandra Devulsky da Silva Tisescu (OAB 276493/SP), Beatriz Helena Theophilo (OAB 312093/SP)
#         </span> 
#       </td>
#     </tr>
#   </tbody>
#   <tbody style="display: none;" id="tabelaTodasMovimentacoes">
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/07/2017
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Arquivado Provisoriamente
#         <br> 
#         <span style="font-style: italic;">
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         23/04/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         In�cio da Execu��o Juntado
#         <br> 
#         <span style="font-style: italic;">
#         Seq.: 01 - Cumprimento de senten�a
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         11/04/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Certid�o de Publica��o Expedida
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o :0072/2014
#         Data da Disponibiliza��o: 11/04/2014
#         Data da Publica��o: 14/04/2014
#         N�mero do Di�rio: 1631
#         P�gina: 276/283
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/02/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Certid�o de Publica��o Expedida
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o :0036/2014
#         Data da Disponibiliza��o: 20/02/2014
#         Data da Publica��o: 21/02/2014
#         N�mero do Di�rio: 1597
#         P�gina: 264/284
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/02/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remetido ao DJE
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o: 0036/2014
#         Teor do ato: Fls. 368/372: Diante da recomenda��o da Corregedoria Geral da Justi�a do Estado de S�o Paulo com refer�ncia ao Of�cio n� 1795/2013 do Minist�rio P�blico do Estado de S�o Paulo, encaminhem-se os autos ao Minist�rio P�blico, � Primeira Promotoria de Justi�a de Habita��o e Urbanismo da Capital para eventual interesse em manifestar-se nos autos, com urg�ncia. Int
#         Advogados(s): Carlos Roberto Domingues Vieira (OAB 109410/SP), Alessandra Devulsky da Silva Tisescu (OAB 276493/SP), Beatriz Helena Theophilo (OAB 312093/SP)
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         18/02/2014
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-25620710" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=25620710&amp;nmRecursoAcessado=Mero+expediente" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-25620710" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=25620710&amp;nmRecursoAcessado=Mero+expediente" target="_blank"> Mero expediente
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Vistos. Apresente a autora o c�lculo aritm�tico do valor decorrente da compensa��o entre as parcelas pagas pelos r�us e a frui��o indevida do im�vel, considerando o mesmo valor da parcela contratual por cada m�s de frui��o indevida do bem. Ap�s o c�lculo e o dep�sito de eventual cr�dito em favor dos r�us ser� procedida a reintegra��o de posse em favor da autora, conforme v. Ac�rd�o de fls. 205 e embargos declarat�rios de fls. 219/221. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         10/12/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Peti��o Juntada
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         02/09/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Certid�o de Publica��o Expedida
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o :0116/2013
#         Data da Disponibiliza��o: 02/09/2013
#         Data da Publica��o: 03/09/2013
#         N�mero do Di�rio: 1489
#         P�gina: 290/326
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         30/08/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remetido ao DJE
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o: 0116/2013
#         Teor do ato: 1. Fls. 362/364: a peti��o veio desacompanhada da referida guia de recolhimento das dilig�ncias do Oficial de Justi�a. 2. Assim, manifeste-se a exequente em termos de prosseguimento do feito em cinco (5) dias, sob pena de extin��o (v. artigos 267, incisos III e IV, 598 e 794, inciso III, do C�digo de Processo Civil), implicando o sil�ncio em concord�ncia. Int.
#         Advogados(s): Carlos Roberto Domingues Vieira (OAB 109410/SP), Alessandra Devulsky da Silva Tisescu (OAB 276493/SP), Beatriz Helena Theophilo (OAB 312093/SP)
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         27/08/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-9501318" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=9501318&amp;nmRecursoAcessado=Mero+expediente" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-9501318" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=9501318&amp;nmRecursoAcessado=Mero+expediente" target="_blank"> Mero expediente
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         1. Fls. 362/364: a peti��o veio desacompanhada da referida guia de recolhimento das dilig�ncias do Oficial de Justi�a. 2. Assim, manifeste-se a exequente em termos de prosseguimento do feito em cinco (5) dias, sob pena de extin��o (v. artigos 267, incisos III e IV, 598 e 794, inciso III, do C�digo de Processo Civil), implicando o sil�ncio em concord�ncia. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         29/07/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Peti��o Juntada
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         14/06/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remetido ao DJE
#         <br> 
#         <span style="font-style: italic;">
#         Rela��o: 0068/2013
#         Teor do ato: 
#         Vistos.
#         Fls. 311/353: diante do que consta dos autos, manifeste-se a autora exequente em termos de prosseguimento do feito em cinco (5) dias. 
#         Int.
#         S�o Paulo, 27 de maio de 2013.
#         Advogados(s): Carlos Roberto Domingues Vieira (OAB 109410/SP), Alessandra Devulsky da Silva Tisescu (OAB 276493/SP)
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         12/06/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Mero expediente
#         <br> 
#         <span style="font-style: italic;">
#         Vistos.
#         Fls. 311/353: diante do que consta dos autos, manifeste-se a autora exequente em termos de prosseguimento do feito em cinco (5) dias. 
#         Int.
#         S�o Paulo, 27 de maio de 2013.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         30/04/2013
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Peti��o Juntada
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         20/10/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Mudan�a de Classe Processual
#         <br> 
#         <span style="font-style: italic;">
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         24/09/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         22
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         24/09/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1  Fls. 301/307: Aguarde-se em Cart�rio, por sessenta (60) dias, o desfecho do recurso interposto.  Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/09/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/09/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354088" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354088&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354088" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354088&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1  Fls. 301/307: Aguarde-se em Cart�rio, por sessenta (60) dias, o desfecho do recurso interposto.  Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/08/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         10/07/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         imprensa certificando rita
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         02/07/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 292/293 e 295/6: Os r�us t�m raz�o. A reintegra��o de posse s� ser� deferida mediante cau��o.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         29/06/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354087" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354087&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354087" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354087&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 292/293 e 295/6: Os r�us t�m raz�o. A reintegra��o de posse s� ser� deferida mediante cau��o. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         29/05/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         18/04/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         P. 04
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         18/04/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         2
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         17/04/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1   Fl. 288: Diga a autora, em quarenta e oito (48) horas, publicando-se com urg�ncia (v. fls. 278 e 287). Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         13/04/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         IMP.R.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         30/03/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354085" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354085&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354085" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354085&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1   Fl. 288: Diga a autora, em quarenta e oito (48) horas, publicando-se com urg�ncia (v. fls. 278 e 287). Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/03/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Digita��o
#         <br> 
#         <span style="font-style: italic;">
#         DAT. R.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         05/03/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         03/02/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         4 4
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         01/02/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1    Fls. 278/279: Antes, informe a autora quanto ao julgamento do Recurso Especial, indicado a fl. 273, diligenciando em dez (10) dias e voltem para exame. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         20/01/2012
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         RITA-15
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         14/12/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         IMP. R.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         13/12/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354084" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354084&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354084" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354084&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1    Fls. 278/279: Antes, informe a autora quanto ao julgamento do Recurso Especial, indicado a fl. 273, diligenciando em dez (10) dias e voltem para exame. Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/10/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         PRONTA.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         19/09/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         P.20
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         15/09/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         1.- Autos baixados da Egr�gia Superior Inst�ncia, com apenas um volume e Recurso Especial. 2.- Diante do que consta dos autos, manifeste-se a parte vencedora em termos de prosseguimento em cinco (5) dias, sob pena de extin��o (v. artigos 267, incisos III e IV, 598 e 794, inciso III, do C�digo de Processo Civil), implicando o sil�ncio em concord�ncia (v. fls. 101/104, 205/206 e 268/273).  3.- Regularizem-se no Sistema os nomes dos Patronos das partes. 4.- Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         13/09/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Digita��o
#         <br> 
#         <span style="font-style: italic;">
#         DAT. R.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         09/09/2011
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354083" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354083&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354083" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354083&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         1.- Autos baixados da Egr�gia Superior Inst�ncia, com apenas um volume e Recurso Especial. 2.- Diante do que consta dos autos, manifeste-se a parte vencedora em termos de prosseguimento em cinco (5) dias, sob pena de extin��o (v. artigos 267, incisos III e IV, 598 e 794, inciso III, do C�digo de Processo Civil), implicando o sil�ncio em concord�ncia (v. fls. 101/104, 205/206 e 268/273).  3.- Regularizem-se no Sistema os nomes dos Patronos das partes. 4.- Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         22/01/2010
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remessa ao Setor
#         <br> 
#         <span style="font-style: italic;">
#         Remetido ao tribunal de justi�a, direito privado 1, complexo do Ipiranga-sala 45, com 01 volumes, carga n�23/10, escrevente responsavel Rita matricula: 317.662-2.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/01/2010
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias DAT R URG
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         11/11/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o Pronta
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         23/10/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Prazo 04 - Aguardando Prazo
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         15/10/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Manifesta��o do Autor
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Manifesta��o do Autor
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         07/10/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 000.2006.162771-1   Fls. 126/129:  Recebo o recurso de Apela��o em ambos os efeitos. �s contrarraz�es, no prazo legal.  Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         06/10/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         Imp. R - Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         02/10/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019637" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019637&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019637" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019637&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 000.2006.162771-1   Fls. 126/129:  Recebo o recurso de Apela��o em ambos os efeitos. �s contrarraz�es, no prazo legal.  Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         09/09/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         01/09/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Conclusos
#         <br> 
#         <span style="font-style: italic;">
#         Conclusos para &lt; Destino &gt;
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         23/06/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o. JUNTADA PRONTA.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         09/06/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         1.- Fls. 111/118:  Recebo o recurso de Apela��o em ambos os efeitos.  �s contrarraz�es, no prazo legal. 2.- Fls. 122/125:  Indefiro o pedido posto que o requerido exerce fun��o remunerada e, al�m disso constituiu banca particular de Advocacia para o patroc�nio de seus interesses. Ao que consta dos autos n�o pode ser havido como pessoa ?pobre? na acep��o jur�dica do termo. Assim, indefiro o benef�cio da Justi�a gratuita com fundamento no artigo 5�, inciso LXXIV, da Constitui��o da Rep�blica. Providencie pois, o requerido em cinco (5) dias o recolhimento das custas de preparo sob pena de deser��o. 3.- Recolhido o valor do preparo, voltem conclusos para aprecia��o (v. fls. 126/129).
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         08/06/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         02/06/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019596" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019596&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019596" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019596&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         1.- Fls. 111/118:  Recebo o recurso de Apela��o em ambos os efeitos.  �s contrarraz�es, no prazo legal. 2.- Fls. 122/125:  Indefiro o pedido posto que o requerido exerce fun��o remunerada e, al�m disso constituiu banca particular de Advocacia para o patroc�nio de seus interesses. Ao que consta dos autos n�o pode ser havido como pessoa ?pobre? na acep��o jur�dica do termo. Assim, indefiro o benef�cio da Justi�a gratuita com fundamento no artigo 5�, inciso LXXIV, da Constitui��o da Rep�blica. Providencie pois, o requerido em cinco (5) dias o recolhimento das custas de preparo sob pena de deser��o. 3.- Recolhido o valor do preparo, voltem conclusos para aprecia��o (v. fls. 126/129). 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         03/04/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         ver. peti.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         11/03/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         16/02/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o &lt; N.� da Peti��o &gt; em
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         13/02/2009
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o &lt; N.� da Peti��o &gt; em
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         22/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Expedi��o
#         <br> 
#         <span style="font-style: italic;">
#         Dat. R - Aguardando Expedi��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         18/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o &lt; N.� da Peti��o &gt; em
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         16/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         15� VARA C�VEL CENTRAL Processo n� 583.00.2006.162771-1 Vistos.  COMPANHIA METROPOLITANA DE HABITA��O DE S�O PAULO COHAB SP move ?A��o de Rescis�o Contratual c/c Reintegra��o de Posse com Pedido de Antecipa��o da Tutela Perdas e Danos? contra, CRESO RONALDO DOMINGUES VIEIRA e SILVIA REGINA RESQUIM VIEIRA alegando, em s�ntese, que compromissou im�vel � venda aos r�us, transmitindo-lhes imediatamente a posse provis�ria, entretanto eles deixaram de adimplir as presta��es mensais pactuadas restando um d�bito no valor de R$ 122.796,27, referente ao per�odo de 10/05/1997 a 10/10/2005. Assim, fazia jus � rescis�o do contrato com a reintegra��o na posse do im�vel e condena��o dos r�us a perca das presta��es pagas a t�tulo de indeniza��o de perdas e danos. Pelo que exp�s requereu a proced�ncia da a��o,  com a concess�o de liminar para reintegra��o na posse do im�vel e a final a proced�ncia da a��o, coma rescis�o do contrato e reintegra��o definitiva, condenando-se os r�us a indeniza��o de perdas e danos consistentes na perda das parcelas atuais.  Juntou os documentos de fls. 09/49. Os r�us contestaram a fls. 57 arg�indo car�ncia da a��o, por falta de notifica��o, acrescentando que n�o tinha ci�ncia do valor do saldo devedor que pretendiam quit�-lo, somente podendo reintegrar-se a autora na posse do im�vel ap�s a restitui��o dos valores pagos pelos compromiss�rios compradores, culminando com o pedido de improced�ncia da a��o.  R�plica a fls. 71. � o relat�rio. Decido. -I-  Trata-se de a��o de rescis�o de compromisso de compra e venda com pedido de reintegra��o de posse e perda das presta��es pagas pelos compromiss�rios compradores inadimplentes.  A preliminar de aus�ncia de notifica��o dos r�us em mora n�o se sustenta, e diante da notifica��o de fls. 40/44 dos autos, que � suficientemente clara acerca do valor do d�bito ? R$ 122.796,27 e que foi enviada ao endere�o dos devedores.  No m�rito, havendo d�bito confesso e tendo sido devidamente notificados os compromiss�rios compradores do im�vel descrito na inicial, imp�e-se mesmo a rescis�o do contrato e conseq�ente reintegra��o de posse da vendedora.  Entretanto, � mat�ria h� muito pacificada pela jurisprud�ncia a impossibilidade integral das presta��es pagas pelos compradores inadimplentes em hip�teses como a dos autos, diante do disposto no art. 53 do C�digo de Defesa do Consumidor. Deve estabelecer-se, portanto, o cumprimento de cl�usula penal. Para tanto, considerando que os requeridos ocuparam o im�vel desde 01 de abril de 2006, data em que foram constitu�dos em mora,  sem o devido pagamento, considero razo�vel  a perda de 40% dos valores pagos � vendedora. Da� a proced�ncia parcial da a��o, com a rescis�o do contrato, mas determinando-se � autora que devolva aos r�us 60% dos valores deles recebidos,  com juros e corre��o monet�ria. -II-  Ante o exposto, JULGO PARCIALMENTE PROCEDENTE a presente a��o para rescindir o contrato firmado pelas partes e reintegrar a autora na posse do im�vel, desde que restitua aos requeridos 60% das import�ncias deles recebidas corrigidas desde o desembolso e acrescidas de juros de mora desde a publica��o desta senten�a.Cada parte arcar� com metade das custas processuais e honor�rios de seu advogado. A taxa judici�ria para apela��o � de R$ 150,38, e o porte de remessa e retorno para 1 volume � de R$ 20,96.                        P.R.I. S�o Paulo, 03 de dezembro de 2008.                        CELINA DIETRICH E TRIGUEIROS TEIXEIRA PINTO                 Ju�za de Direito
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         11/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         R.10
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         09/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Senten�a  Registrada
#         <br> 
#         <span style="font-style: italic;">
#         N�mero Senten�a: 3374/2008
#         Livro: 632
#         Folha(s): de 72 at� 74
#         Data Registro: 09/12/2008 16:23:24
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         05/12/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354089" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354089&amp;nmRecursoAcessado=Senten%C3%A7a++Proferida" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354089" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354089&amp;nmRecursoAcessado=Senten%C3%A7a++Proferida" target="_blank"> Senten�a  Proferida
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Senten�a n� 3374/2008 registrada em 09/12/2008 no livro n� 632 �s Fls. 72/74: Ante o exposto, JULGO PARCIALMENTE PROCEDENTE a presente a��o para rescindir o contrato firmado pelas partes e reintegrar a autora na posse do im�vel, desde que restitua aos requeridos 60% das import�ncias deles recebidas corrigidas desde o desembolso e acrescidas de juros de mora desde a publica��o desta senten�a.Cada parte arcar� com metade das custas processuais e honor�rios de seu advogado. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         07/11/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1  Regularizem-se no Sistema os nomes dos Patronos das Partes, proceda-se � confer�ncia da numera��o dos autos e voltem para o sentenciamento, diligenciando o Cart�rio.   Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         05/11/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Expedi��o
#         <br> 
#         <span style="font-style: italic;">
#         Dat. R - Aguardando Expedi��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         04/11/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         03/11/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019595" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019595&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019595" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019595&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 2006.162771-1  Regularizem-se no Sistema os nomes dos Patronos das Partes, proceda-se � confer�ncia da numera��o dos autos e voltem para o sentenciamento, diligenciando o Cart�rio.   Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         26/09/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Prazo 24 - Aguardando Prazo
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         29/08/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         24
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         29/07/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 583.00.2006.162771-1  Vistos.    A preliminar arg�ida na contesta��o entrela�a-se com o m�rito e ser� decidida na senten�a. Consertados, conclusos para senten�a.  Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         24/07/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         C.I. R - Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         18/07/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354001" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354001&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354001" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354001&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 583.00.2006.162771-1  Vistos.    A preliminar arg�ida na contesta��o entrela�a-se com o m�rito e ser� decidida na senten�a. Consertados, conclusos para senten�a.  Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         03/07/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Conclusos
#         <br> 
#         <span style="font-style: italic;">
#         Conclusos para &lt; Destino &gt;
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         02/07/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Juntada
#         <br> 
#         <span style="font-style: italic;">
#         pronta
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         10/06/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o (pronta-mesa)
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         03/06/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Prazo 04 - Aguardando Prazo
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         20/05/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias - mesa chefe
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         12/05/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Traslado de Pe�as
#         <br> 
#         <span style="font-style: italic;">
#         XEROX
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         24/04/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 583.00.2006.162771-1    Fls.79, 80/81, 82, 84, 86, 88, 91, 93 e 94: Diante do sil�ncio da autora na fase de especifica��o de provas, manifestem-se os r�us se insistem nas provas especificadas, em cinco (5) dias ou pretendem o julgamento antecipado da lide, implicando o sil�ncio em concord�ncia.  Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         22/04/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         IMP. R.05
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         17/04/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35354000" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354000&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35354000" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35354000&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Processo n� 583.00.2006.162771-1    Fls.79, 80/81, 82, 84, 86, 88, 91, 93 e 94: Diante do sil�ncio da autora na fase de especifica��o de provas, manifestem-se os r�us se insistem nas provas especificadas, em cinco (5) dias ou pretendem o julgamento antecipado da lide, implicando o sil�ncio em concord�ncia.  Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         14/04/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Prazo 11
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         05/03/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         16
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         22/01/2008
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Prazo
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         08/11/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Prazo
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         31/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         IMP " 1 " - Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         30/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls.90-Manifeste-se o autor em termos de prosseguimento em cinco dias.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         29/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Juntada de Peti��o
#         <br> 
#         <span style="font-style: italic;">
#         Juntada da Peti��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         27/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-35353999" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35353999&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-35353999" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=35353999&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Fls.90-Manifeste-se o autor em termos de prosseguimento em cinco dias. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         24/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Publica��o
#         <br> 
#         <span style="font-style: italic;">
#         IMP " G " - Aguardando Publica��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         19/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Conclusos
#         <br> 
#         <span style="font-style: italic;">
#         CONCLUS�O
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         17/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         Aguardando Provid�ncias
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         05/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Retorno do Setor
#         <br> 
#         <span style="font-style: italic;">
#         Recebido do Setor de Concilia��o em 05.10.2007 e, encaminhado para se��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         02/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Audi�ncia
#         <br> 
#         <span style="font-style: italic;">
#         PODER JUDICI�RIO
#         TRIBUNAL DE JUSTI�A DO ESTADO DE S�O PAULO
#         Comarca  S�o Paulo - Foro Central C�vel 
#         15� Vara C�vel  - Setor de  Concilia��o 
#         Pra�a Jo�o Mendes s/n�, 21 andar - salas n� 2109, Centro, - CEP 01501-900, S�o Paulo-SP   
#         Processo n� 583.00.2006.162771-1      ORDEM  870/ 2006
#         TERMO DE AUDI�NCIA
#         A��o:   Possess�rias em geral
#         Reqte:      COMPANHIA METROPOLITANA DE HABITA��O DE S�O PAULO ? COHAB SP
#         Adv reqte:  ausente
#         Reqdo:  CRESO RONALDO DOMINGUES VIEIRA ? RG 14835000-8 - presente
#         Reqdo:  SILVIA REGINA RESQUIM VIEIRA ? ausente
#         Adv reqdos: Dr. CARLOS ROBERTO DOMINGUES VIEIRA ? OAB 109410 - presente
#         Aos    02 de outubro de 2007, �s    10:40  horas, nesta cidade e Comarca  S�o Paulo, na sala de audi�ncia do SETOR DE CONCILIA��O, sob a presen�a do(a) conciliador(a) Leila Teixeira de Arruda, comigo Escrevente abaixo assinado, compareceram os acima mencionados. Abertos os trabalhos restou prejudicada a concilia��o ante a aus�ncia do(s) requerente (s) ou de quem o(s) representasse(m). Pelo(a) Conciliador(a) foi consignado o retorno dos autos � Vara de origem.. Nada mais. Lido e achado conforme, vai devidamente assinado.  Eu,_________,(Solange Ramos Salzano Rossi), Escrevente T�cnico Judici�rio, digitei.
#         Conciliador (a): 
#         Requerido:
#         Repres. reqdos: sol
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         01/10/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Remessa ao Setor
#         <br> 
#         <span style="font-style: italic;">
#         Remetido ao Setor Concilia��o
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         24/09/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         02
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         21/09/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Prazo
#         <br> 
#         <span style="font-style: italic;">
#         Prazo 02
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         13/08/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019594" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019594&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019594" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019594&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Nos termos da Ordem de servi�o n� 01/2004, fica designada audi�ncia de concilia��o para o dia 02 de Outubro de 2007, �s 10:40 horas, a ser realizada no Setor de Concilia��o do F�rum Jo�o Mendes J�nior, situado na P�a Jo�o Mendes, s/n�, 21� andar, sala 2109.? Consigna-se que, nos termos da Ordem de Servi�o n� 06/2.005, e das disposi��es do artigo 14, inciso II do CPC, � dever do advogado agir com lealdade processual e boa f�, bem como, e ainda, conforme preceituado no artigo 17, inciso IV do CPC, o bom andamento processual n�o deve ser obstaculizado injustificadamente , o que  implica, em conseq��ncia , no comparecimento � audi�ncia ora designada , n�o s� porque  atender �s intima��es para comparecimento em audi�ncias represente conduta de lealdade processual, como tamb�m, a aus�ncia �s mesmas, injustificadamente , representar�  obst�culo ao bom andamento processual, em vista do tempo e trabalho havidos pelo Ju�zo com a referida designa��o. Ademais, registra-se que considerando que a concilia��o atende, induvidosamente , o interesse p�blico, e sendo dever  �tico do advogado estimular a concilia��o entre as partes, conforme disposi��o do artigo 2�, par�grafo �nico, incisos II e VI do C�digo de �tica da Ordem dos Advogados do Brasil, O COMPARECIMENTO DO ADVOGADO E DAS PARTES � OBRIGAT�RIO, sendo que eventual aus�ncia dever� ser jistificada documentalmente , em cinco dias. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         13/08/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 86 - Nos termos da Ordem de servi�o n� 01/2004, fica designada audi�ncia de concilia��o para o dia 02 de Outubro de 2007, �s 10:40 horas, a ser realizada no Setor de Concilia��o do F�rum Jo�o Mendes J�nior, situado na P�a Jo�o Mendes, s/n�, 21� andar, sala 2109.? Consigna-se que, nos termos da Ordem de Servi�o n� 06/2.005, e das disposi��es do artigo 14, inciso II do CPC, � dever do advogado agir com lealdade processual e boa f�, bem como, e ainda, conforme preceituado no artigo 17, inciso IV do CPC, o bom andamento processual n�o deve ser obstaculizado injustificadamente , o que  implica, em conseq��ncia , no comparecimento � audi�ncia ora designada , n�o s� porque  atender �s intima��es para comparecimento em audi�ncias represente conduta de lealdade processual, como tamb�m, a aus�ncia �s mesmas, injustificadamente , representar�  obst�culo ao bom andamento processual, em vista do tempo e trabalho havidos pelo Ju�zo com a referida designa��o. Ademais, registra-se que considerando que a concilia��o atende, induvidosamente , o interesse p�blico, e sendo dever  �tico do advogado estimular a concilia��o entre as partes, conforme disposi��o do artigo 2�, par�grafo �nico, incisos II e VI do C�digo de �tica da Ordem dos Advogados do Brasil, O COMPARECIMENTO DO ADVOGADO E DAS PARTES � OBRIGAT�RIO, sendo que eventual aus�ncia dever� ser jistificada documentalmente , em cinco dias.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         06/08/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 86 - Fls 257: Nos termos da Ordem de servi�o n� 01/2004, fica designada audi�ncia de concilia��o para o dia 02 de outubro de 2007, �s 10:40 horas,   a ser realizada no Setor de Concilia��o do F�rum Jo�o Mendes J�nior, situado na Pra�a Jo�o Mendes, s/n�, 21� andar, sala 2109. ?Consigna-se que, nos termos da Ordem de Servi�o n� 06/2.005, e das disposi��es do artigo 14, inciso II do CPC, � dever do advogado agir com lealdade processual e boa f�, bem como, e ainda, conforme preceituado no artigo 17, inciso IV do CPC, o bom andamento processual n�o deve ser obstaculizado injustificadamente, o que implica, em conseq��ncia, no comparecimento � audi�ncia ora designada, n�o s� porque atender �s intima��es para comparecimento em audi�ncias represente conduta de lealdade processual, como tamb�m, � aus�ncia �s mesmas, injustificadamente , representar� obst�culo ao bom andamento processual, em vista do tempo e trabalho havidos pelo Ju�zo com a referida designa��o. Ademais registra-se  que considerando  que a concilia��o atende,  induvidosamente , o interesse p�blico, e sendo dever �tico do advogado estimular a  concilia��o entre as partes, conforme disposi��o do artigo 2� , par�grafo �nico , incisos II e VI do C�digo de �tica da Ordem  dos Advogados do Brasil, O COMPARECIMENTO DO ADVOGADO E DAS PARTES � OBRIGAT�RIO, sendo que eventual aus�ncia dever� ser justificada documentalmente , em cinco dias.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         01/08/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019593" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019593&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019593" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019593&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Fls 257: Nos termos da Ordem de servi�o n� 01/2004, fica designada audi�ncia de concilia��o para o dia 02 de outubro de 2007, �s 10:40 horas,   a ser realizada no Setor de Concilia��o do F�rum Jo�o Mendes J�nior, situado na Pra�a Jo�o Mendes, s/n�, 21� andar, sala 2109. ?Consigna-se que, nos termos da Ordem de Servi�o n� 06/2.005, e das disposi��es do artigo 14, inciso II do CPC, � dever do advogado agir com lealdade processual e boa f�, bem como, e ainda, conforme preceituado no artigo 17, inciso IV do CPC, o bom andamento processual n�o deve ser obstaculizado injustificadamente, o que implica, em conseq��ncia, no comparecimento � audi�ncia ora designada, n�o s� porque atender �s intima��es para comparecimento em audi�ncias represente conduta de lealdade processual, como tamb�m, � aus�ncia �s mesmas, injustificadamente , representar� obst�culo ao bom andamento processual, em vista do tempo e trabalho havidos pelo Ju�zo com a referida designa��o. Ademais registra-se  que considerando  que a concilia��o atende,  induvidosamente , o interesse p�blico, e sendo dever �tico do advogado estimular a  concilia��o entre as partes, conforme disposi��o do artigo 2� , par�grafo �nico , incisos II e VI do C�digo de �tica da Ordem  dos Advogados do Brasil, O COMPARECIMENTO DO ADVOGADO E DAS PARTES � OBRIGAT�RIO, sendo que eventual aus�ncia dever� ser justificada documentalmente , em cinco dias. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         23/07/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Retorno do Setor
#         <br> 
#         <span style="font-style: italic;">
#         Recebido do setor de concilia��o em 23/07(mesa do Franklyn)
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         20/07/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Audi�ncia
#         <br> 
#         <span style="font-style: italic;">
#         Audi�ncia designada: Nos termos da Ordem de Servi�o N� 01/2004, fica designada audi�ncia de concilia��o para o dia 02/10/2007, �s  10:40 horas, a  ser realizada F�rum Jo�o Mendes J�nior, situado na Pra�a Jo�o Mendes, s/n, 8�andar, sala_825. Certifico que as partes ficam intimadas da designa��o com a publica��o deste, ?Consigna-se que, nos termos da Ordem de Servi�o n� 06/2.005, e das disposi��es do artigo 14, inciso II do CPC, � dever do advogado agir com lealdade processual e boa f�, bem como, e ainda, conforme preceituado no artigo 17, inciso IV do CPC, o bom andamento processual n�o deve ser obstaculizado injustificadamente, o que implica, em conseq��ncia, no comparecimento � audi�ncia ora designada, n�o s� porque atender �s intima��es para comparecimento em audi�ncias represente conduta de lealdade processual, como tamb�m, a aus�ncia �s mesmas, injustificadamente, representar� obst�culo ao bom andamento processual, em vista do tempo e trabalho havidos pelo Ju�zo com a referida designa��o. Ademais, registra-se que considerando que a concilia��o atende, induvidosamente, o interesse p�blico, e sendo dever �tico do advogado estimular a concilia��o entre as partes, conforme disposi��o do artigo 2�, par�grafo �nico, incisos II e VI do C�digo de �tica da Ordem dos Advogados do Brasil, O COMPARECIMENTO DO ADVOGADO E DAS PARTES � OBRIGAT�RIO, sendo que eventual aus�ncia dever� ser justificada documentalmente, em cinco dias. Dulce
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         24/05/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Aguardando Provid�ncias
#         <br> 
#         <span style="font-style: italic;">
#         RECEBI NO SETOR EM 24/05/07. ESTER
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         03/05/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 84 - Ao Setor de Concilia��o. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         27/04/2007
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019592" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019592&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019592" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019592&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Ao Setor de Concilia��o. Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         14/12/2006
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 79 - Especifiquem provas, justificando-as, em cinco (5) dias. Sem preju�zo, digam se h� interesse na audi�ncia de concilia��o. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         13/12/2006
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019639" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019639&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019639" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019639&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Especifiquem provas, justificando-as, em cinco (5) dias. Sem preju�zo, digam se h� interesse na audi�ncia de concilia��o. Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         04/09/2006
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Data da Publica��o SIDAP
#         <br> 
#         <span style="font-style: italic;">
#         Fls. 68 - Fls 57/64: � r�plica. Int.
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoEscuro" style="">
#       <td width="120" style="vertical-align: top">
#         31/08/2006
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#         <a class="linkMovVincProc" id="linkMovVincProc-37019638" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019638&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank">
#         <img width="16" height="16" border="0" src="/cpopg/imagens/doc2.gif">
#         </a>
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         <a class="linkMovVincProc" id="linkMovVincProc-2-37019638" title="Visualizar documento em inteiro teor" href="/cpopg/abrirDocumentoVinculadoMovimentacao.do?processo.codigo=2SZX6F13N0000&amp;cdDocumento=37019638&amp;nmRecursoAcessado=Despacho+Proferido" target="_blank"> Despacho Proferido
#         </a>
#         <br> 
#         <span style="font-style: italic;">
#         Fls 57/64: � r�plica. Int. 
#         </span> 
#       </td>
#     </tr>
#     <tr class="fundoClaro" style="">
#       <td width="120" style="vertical-align: top">
#         09/06/2006
#       </td>
#       <td width="20" valign="top" aria-hidden="true">
#       </td>
#       <td style="vertical-align: top; padding-bottom: 5px">
#         Processo Distribu�do
#         <br> 
#         <span style="font-style: italic;">
#         Processo Distribu�do por Sorteio p/ 15�. Vara C�vel
#         </span> 
#       </td>
#     </tr>
#   </tbody>
# </table>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Peti��es diversas
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <!-- Tabela de peti��es diversas -->
# <table style="margin-left:15px; margin-top:1px;" align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
#   <thead>
#     <tr class="label">
#       <th width="140" style="text-align:left">Data</th>
#       <th width="*">Tipo</th>
#     </tr>
#     <tr class="fundoEscuro" height="2" aria-hidden="true">
#       <td></td>
#       <td></td>
#     </tr>
#   </thead>
#   <tbody>
#     <tr class="fundoClaro">
#       <td width="140" style="text-align:left">
#         27/04/2015
#       </td>
#       <td width="*">
#         Peti��es Diversas <br>
#       </td>
#     </tr>
#     <tr class="fundoEscuro">
#       <td width="140" style="text-align:left">
#         16/02/2018
#       </td>
#       <td width="*">
#         Peti��es Diversas <br>
#       </td>
#     </tr>
#     <tr class="fundoClaro">
#       <td width="140" style="text-align:left">
#         19/02/2018
#       </td>
#       <td width="*">
#         Peti��es Diversas <br>
#       </td>
#     </tr>
#     <tr class="fundoEscuro">
#       <td width="140" style="text-align:left">
#         22/02/2018
#       </td>
#       <td width="*">
#         Peti��es Diversas <br>
#       </td>
#     </tr>
#   </tbody>
# </table>
# <!--  Tabela de peti��es diversas -->
# <script type="text/javascript">
#   (function($) {
#     $(function(){
#         var captcha = $.saj.getUrlParameter('uuidCaptcha');
#         if(!captcha){
#             return;
#         }
#         $('.incidente a').each(function(){
#             var $incidente = $(this);
#             var url = $incidente.attr('href');
#             $incidente.attr('href', url+'&uuidCaptcha='+captcha);
#         });
#     })
#   })(jQuery);
# </script>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Incidentes, a��es incidentais, recursos e execu��es de senten�as
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <table style="margin-left:15px; margin-top:1px;" align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
#   <tbody>
#     <tr class="label">
#       <th width="140">
#         Recebido em
#       </th>
#       <th width="*" class="label">
#         Classe
#       </th>
#     </tr>
#     <tr class="fundoEscuro" height="2" aria-hidden="true">
#       <td></td>
#       <td></td>
#     </tr>
#     <tr class="fundoClaro">
#       <td width="140" style="text-align:left">
#         23/04/2014
#       </td>
#       <td width="*">
#         <a href="/cpopg/show.do?localPesquisa.cdLocal=100&amp;processo.codigo=2SZX6F13N0001&amp;processo.foro=100" target="_top">
#         Cumprimento de senten�a
#         - 00001
#         </a>
#       </td>
#     </tr>
#   </tbody>
# </table>
# <!--  Incidentes -->
# <script type="text/javascript">
#   (function ($) {
#       $(function () {
#           var captcha = $.saj.getUrlParameter('uuidCaptcha');
  
#           if(!captcha){
#               return;
#           }
#           $.each($('.processoApensado'), function (i, value) {
#               var $link = $(value);
#               $link.attr('href', $link.attr('href') + '&uuidCaptcha=' + captcha);
#           })
  
#       })
#   })(jQuery);
# </script>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Apensos, Entranhados e Unificados
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <table style="margin-left:15px; margin-top:1px;" align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
#   <tbody id="dadosApensosNaoDisponiveis">
#     <tr>
#       <td colspan="3" align="left">N�o h� processos apensados, entranhados e unificados a este processo.</td>
#     </tr>
#   </tbody>
# </table>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Audi�ncias
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <a name="audienciasPlaceHolder"></a>
# <table style="margin-left:15px; margin-top:1px;" align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
#   <tbody>
#     <tr class="label">
#       <th align="left" valign="top" width="140">Data</th>
#       <th align="left" valign="top" width="*">Audi�ncia</th>
#       <th align="left" valign="top" width="100">Situa��o</th>
#       <th align="left" valign="top" width="100">Qt. Pessoas</th>
#     </tr>
#     <tr class="fundoEscuro" height="2" aria-hidden="true">
#       <td></td>
#       <td></td>
#       <td></td>
#       <td></td>
#     </tr>
#     <tr class="fundoClaro">
#       <td valign="top" align="left" width="140">
#         02/10/2007
#       </td>
#       <td valign="top" align="left" width="*">
#         Concilia��o
#       </td>
#       <td align="left" valign="top" width="100">
#         Pendente
#       </td>
#       <td align="left" valign="top" width="100">
#         0
#       </td>
#     </tr>
#   </tbody>
# </table>
# <div style="padding-top: 10px;">
#   <table width="100%" border="0" cellspacing="0" cellpadding="0">
#     <tbody>
#       <tr valign="top">
#         <td height="21" valign="top" nowrap="" background="/cpopg/imagens/spw/fundo_subtitulo.gif">
#           <h2 class="subtitle">
#             Hist�rico de classes
#           </h2>
#         </td>
#         <td background="/cpopg/imagens/spw/fundo_subtitulo2.gif" width="90%" aria-hidden="true" valign="top">
#           <img src="/cpopg/imagens/spw/final_subtitulo.gif" width="16" height="20" tabindex="-1">
#         </td>
#       </tr>
#     </tbody>
#   </table>
# </div>
# <table style="margin-left:15px; margin-top:1px;" align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
#   <thead>
#     <tr class="label">
#       <th align="left" valign="top" width="150">Data</th>
#       <th align="left" valign="top" width="120">Tipo</th>
#       <th align="left" valign="top" width="300">Classe</th>
#       <th align="left" valign="top" width="120">�rea</th>
#       <th align="left" valign="top" width="*">Motivo</th>
#     </tr>
#     <tr class="fundoEscuro" height="2" aria-hidden="true">
#       <td></td>
#       <td></td>
#       <td></td>
#       <td></td>
#       <td></td>
#     </tr>
#   </thead>
#   <tbody>
#     <tr class="fundoClaro">
#       <td valign="top" align="left" width="150">
#         01/05/2012
#       </td>
#       <td align="left" valign="top" width="120">
#         Inicial
#       </td>
#       <td align="left" valign="top" width="300">
#         Possess�rias em Geral(Reintegra��o, Manuten��o, Interdito)
#       </td>
#       <td align="left" valign="top" width="120">
#         C�vel
#       </td>
#       <td align="left" valign="top" width="*">
#         -
#       </td>
#     </tr>
#   </tbody>
#   <tbody>
#     <tr class="fundoEscuro">
#       <td valign="top" align="left" width="150">
#         01/05/2012
#       </td>
#       <td align="left" valign="top" width="120">
#         Corre��o
#       </td>
#       <td align="left" valign="top" width="300">
#         Reintegra��o / Manuten��o de Posse
#       </td>
#       <td align="left" valign="top" width="120">
#         C�vel
#       </td>
#       <td align="left" valign="top" width="*">
#         -
#       </td>
#     </tr>
#   </tbody>
#   <tbody>
#     <tr class="fundoClaro">
#       <td valign="top" align="left" width="150">
#         20/10/2012
#       </td>
#       <td align="left" valign="top" width="120">
#         Evolu��o
#       </td>
#       <td align="left" valign="top" width="300">
#         Reintegra��o / Manuten��o de Posse
#       </td>
#       <td align="left" valign="top" width="120">
#         C�vel
#       </td>
#       <td align="left" valign="top" width="*">
#         -
#       </td>
#     </tr>
#   </tbody>
# </table>
# <form id="popupCdas" style="display: none;">
#   <!--  dados da lista (CDAs) -->
#   <div style="height:210px; overflow: auto;">
#     <table id="tableCdasPrincipais" style="margin-left:10px; margin-top:1px; width: 98%;">
#       <thead>
#         <tr class="fundoClaro">
#           <th style="text-align:left;">N�mero CDA</th>
#           <th style="text-align:left;">Valor</th>
#           <th style="text-align:left;">Dt. CDA</th>
#           <th style="text-align:left;">Valor atualizado</th>
#           <th style="text-align:left;">Dt. atualiza��o</th>
#           <th style="text-align:left;">Situa��o</th>
#         </tr>
#         <tr class="fundoEscuro" height="2" aria-hidden="true">
#           <td colspan="6">
#           </td>
#         </tr>
#       </thead>
#       <tbody>
#       </tbody>
#     </table>
#   </div>
# </form>
# <table id="" class="secaoBotoesBody" width="100%" style="" cellpadding="2" cellspacing="0" border="0">
#   <tbody>
#     <tr>
#       <td align="center">
#       </td>
#     </tr>
#   </tbody>
# </table>
# """